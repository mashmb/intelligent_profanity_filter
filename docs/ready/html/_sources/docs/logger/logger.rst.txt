Package bgm.nai.logger
======================

Package responsible for logs appearance.

.. image:: ../../../UML/logger_package.png
	:align: center

=========
LogFormat
=========

.. py:class:: class LogFormat extends Formatter

	Custom logger messages appearance.

	.. py:method:: public String format(LogRecord logRecord)

		Format logger record to customized message.

		:param logRecord: logger record
		:type logRecord: LogRecord
		:returns: formatted logger message
		:rtype: String

============
CustomLogger
============

.. py:class:: CustomLogger

	Customized logger with console handler and custom formatter.

	.. py:method:: public CustomLogger(String className)

		CustomLogger constructor.

		:param className: name of the class that uses logger
		:type className: String

	.. py:method:: public Logger getLogger()

		Get access to customized logger (custom console handler and custom messages appearance).

		:returns: customized logger
		:rtype: Logger