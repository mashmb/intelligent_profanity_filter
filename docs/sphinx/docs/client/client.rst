Package bgm.nai.client
======================

Package responsible for group chat client.

.. image:: ../../../UML/client_package.png
	:align: center

==========
ChatClient
==========

.. py:class:: ChatClient

	Group chat client main entry point

	.. py:method:: public static void main(String[] args)

		Initialize group chat client program.

		:param args: program arguments
		:type args: String[]

=====
Model
=====

.. py:class:: Model

	Data layer for group chat client.

	.. py:method:: public String getLocalhost()

		Get access to localhost IP address.

		:returns: localhost IP address
		:rtype: String

	.. py:method:: public String getNickname()

		Get access to group chat client nickname.

		:returns: group chat client nickname
		:rtype: String

	.. py:method:: public ChatServerInt getRemoteServer()

		Get access to remote group chat server object.

		:returns: remote group chat server object
		:rtype: ChatServerInt

	.. py:method:: public String getServerIp()

		Get access to group chat server IP address.

		:returns: group chat server IP address
		:rtype: String

	.. py:method:: public int getServerPort()

		Get access to group chat server port.

		:returns: group chat server port
		:rtype: int

	.. py:method:: public boolean validateIp(String ip)

		Check if given IP address is correct.

		:param ip: IP address to validate
		:type ip: String
		:returns: logical value if IP address is valid
		:rtype: boolean

	.. py:method:: public void setLocalhost(String localhost)

		Set IP address of group chat client.

		:param localhost: IP address
		:type localhost: String

	.. py:method:: public void setNickname(String nick)

		Set nickname for user that will be chatting.

		:param nick: chosen nickname
		:type nick: String

	.. py:method:: public void setRemoteServer(ChatServerInt serverInterface)

		Set remote group chat server object.

		:param serverInterface: remote group chat server object
		:type serverInterface: ChatServerInt

	.. py:method:: public void setServerIp(String ip)

		Set IP address of group chat server.

		:param ip: IP address
		:type ip: String

====
View
====

.. py:class:: View

	View layer for chat client.

	.. py:method:: public View()

		View constructor and components settings.

	.. py:method:: public MessageScrollPane getMessageScrollPane()

		Get access to scroll pane with text area where new message is created.

		:returns: scroll pane with text area where new message is created
		:rtype: MessageScrollPane

	.. py:method:: public MessagesScrollPane getMessagesScrollPane()

		Get access to scroll pane with text area that contains conversation.

		:returns: scroll pane with text area that contains conversation
		:rtype: MessagesScrollPane

	.. py:method:: public JLabel getNickLabel()

		Get access to label with nickname.

		:returns: label with nickname
		:rtype: JLabel

	.. py:method:: public JButton getSendButton()

		Get access to send button.

		:returns: send button
		:rtype: JButton

	.. py:method:: public Window getWindow()

		Get access to group chat client window.

		:returns: group chat client window
		:rtype: Window

	.. py:method:: private void setUI()

		Set system theme for Java Swing components.

==============
ChatClientImpl
==============

.. py:class:: class ChatClientImpl extends UnicastRemoteObject

	Implementation of group chat client, network layer.

	.. py:method:: protected ChatClientImpl(Model model, View view) throws RemoteException

		ChatClientImpl constructor.

		:param model: access to data layer
		:type model: Model
		:param view: access to view layer
		:type view: View
		:throws RemoteException: exception thrown while problem with network communication occur

	.. py:method:: public void detectInterface()

		Find proper IP address for network communication (proper interface).

	.. py:method:: public void updateMessageBox(String message) throws RemoteException

		Update message box with new message.

		:param message: new message
		:type message: String
		:throws RemoteException: exception thrown while problem with network communication occur

==========
Controller
==========

.. py:class:: Controller

	Control layer for group chat client.

	.. py:method:: public Controller(Model model, View view, ChatClientImpl networkDriver)

		Controller constructor.

		:param model: access to data layer
		:type model: Model
		:param view: access to view layer
		:type view: View
		:param networkDriver: access to network layer
		:type networkDriver: ChatClientImpl

	.. py:method:: public void init()

		Initialize group chat client program and controls listeners.

	.. py:method:: private void connect()

		Connect to group chat server. Before connection initialization server IP address must be validated. If everything goes properly user will bw connected to group chat server. Nickname for every client must be unique.

	.. py:method:: private void disconnect()

		Disconnect from group chat server. Disconnection should be accepted by server.

	.. py:method:: private void forceExit()

		Safety force group chat client program to exit.

	.. py:method:: private void sendMessage(String message)

		Send new message to group chat server.

		:param message: message to send
		:type message: String