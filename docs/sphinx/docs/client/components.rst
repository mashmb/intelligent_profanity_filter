Package bgm.nai.client.components
=================================

Package responsible for group chat client graphical user interface components.

.. image:: ../../../UML/client_components_package.png
	:align: center

======
Window
======

.. py:class:: class Window extends JFrame

	Main window for chat client.

	.. py:method:: public Window()

		Window constructor and settings.

===========
InitialPane
===========

.. py:class:: class InitialPane extends JPanel

	Initial pane to get group chat server IP address and nickname from user.

	.. py:method:: public InitialPane()

		InitialPane constructor and components settings.

	.. py:method:: public JTextField getNicknameField()

		Get access to field where user types their nickname for conversation.

		:returns: field with user nickname
		:rtype: JTextField

	.. py:method:: public JTextField getServerIpField()

		Get access to field where user types group chat server IP address.

		:returns: field with group chat server IP address
		:rtype: JTextField

==================
MessagesScrollPane
==================

.. py:class:: class MessagesScrollPane extends JScrollPane

	Scroll pane with auto scrolling text with messages received from server.

	.. py:method:: public MessagesScrollPane()

		MessagesScrollPane constructor and components settings.

	.. py:method:: public JTextArea getTextarea()

		Get access to text area with conversation.

		:returns: text area with conversation
		:rtype: JTextArea

=================
MessageScrollPane
=================

.. py:class:: class MessageScrollPane extends JScrollPane

	Scroll pane with text area that contains message to send to the chat server.

	.. py:method:: public MessageScrollPane()

		MessageScrollPane constructor and components settings.

	.. py:method:: public JTextArea getTextarea()

		Get access to text area with new message.

		:returns: text area with new message
		:rtype: JTextArea