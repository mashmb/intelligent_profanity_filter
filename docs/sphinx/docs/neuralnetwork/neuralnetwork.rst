Package bgm.nai.neuralnetwork
=============================

Package responsible for feed forward neural network.

.. image:: ../../../UML/neuralnetwork_package.png
	:align: center

======
Neuron
======

.. py:class:: Neuron

	Single neuron representation.

	.. py:method:: public Neuron(int numberOfWeights)

		Neuron constructor.

		:param numberOfWeights: number of neuron weights
		:type numberOfWeights: int

	.. py:method:: public double getError()

		Get access to neuron error.

		:returns: neuron error
		:rtype: double

	.. py:method:: public double getThreshold()

		Get access to neuron threshold.

		:returns: neuron threshold
		:rtype: double

	.. py:method:: public double[] getWeights()

		Get access to neuron weights.

		:returns: neuron weights
		:rtype: double[]

	.. py:method:: public double outputSignal(double[] inputVector)

		Calculate output signal for given vector (constant sigmoid bipolar function).

		:param inputVector: input vector
		:type inputVector: double[]
		:returns: neuron output signal
		:rtype: double

	.. py:method:: private void initialize()

		Set initial neuron state.

	.. py:method:: public void setError(double err)

		Set neuron error.

		:param err: calculated error for neuron
		:type err: double

	.. py:method:: public void setWeights(String[] lineOfWeights)

		Set new weights for neuron.

		:param lineOfWeights: line of weights for neuron read from file
		:type lineOfWeights: String[]

	.. py:method:: public void updateWeights(double learningRate, double[] inputVector)

		Update neuron weights and threshold.

		:param learningRate: neural network learning rate
		:type learningRate: double
		:param inputVector: input vector for neuron
		:type inputVector: double[]

=====
Layer
=====

.. py:class:: class abstract Layer

	Standard layer construction for multilayer neural network.

	.. py:method:: public Layer(int numberOfNeurons, int numberOfWeights)

		Layer constructor.

		:param numberOfNeurons: number of neurons in layer
		:type numberOfNeurons: int
		:param numberOfWeights: number of weights for neurons in layer
		:type numberOfWeights: int

	.. py:method:: public void calculateLayerOutput(double[] inputVector)

		Calculate output values from neurons in layer.

		:param inputVector: input vector for layer
		:type inputVector: double[]

	.. py:method:: private void initialize(int numberOfWeights)

		Initialize layer by creating proper number of neurons in layer.

		:param numberOfWeights: number of weights for neurons in layer
		:type numberOfWeights: int

	.. py:method:: public abstract void neuronsError(double[] answer)

		Calculate error for each neuron in output layer.

		:param answer: proper answer for given training example as vector
		:type answer: double[]

	.. py:method:: public abstract void neuronsError(Neuron[] nextLayer)

		Calculate error for each neuron in layer (hidden layer usage).

		:param nextLayer: next layer in multilayer neural network
		:type nextLayer: Neuron[]

	.. py:method:: public void updateWeights(double learningRate, double[] inputVector)

		Update weights for all neurons in layer.

		:param learningRate: neural network learning rate
		:type learningRate: double
                :param inputVector: input vector for layer
                :type inputVector: double[]

===========
HiddenLayer
===========

.. py:class:: class HiddenLayer extends Layer

	Hidden layer for multilayer neural network.

	.. py:method:: public HiddenLayer(int numberOfNeurons, int numberOfWeights)

		HiddenLayer constructor.

                :param numberOfNeurons: number of neurons in hidden layer
                :type numberOfNeurons: int
                :param numberOfWeights: number of weights for neurons in hidden layer
                :type numberOfWeights: int

	.. py:method:: public void neuronsError(Neuron[] nextLayer)

		Calculate error for each neuron in hidden layer.

		:param nextLayer: next layer in multilayer neural network
		:type nextLayer: Neuron[]

===========
OutputLayer
===========

.. py:class:: class OutputLayer extends Layer

	Output layer for multilayer neural network.

	.. py:method:: public OutputLayer(int numberOfNeurons, int numberOfWeights)

		OutputLayer constructor.

                 :param numberOfNeurons: number of neurons in output layer
                 :type numberOfNeurons: int
                 :param numberOfWeights: number of weights for neurons in output layer
                 :type numberOfWeights: int

	.. py:method:: public void neuronsError(double[] answer)

		Calculate error for each neuron in output layer.

		:param answer: proper answer for given training example as vector
		:type answer: double[]

=============
NeuralNetwork
=============

.. py:class:: NeuralNetwork

	Neural network representation.

	.. py:method:: public NeuralNetwork(int inputLength, int outputLength, List<String> weights, Map<String, Double> trainingModel)

		NeuralNetwork constructor.

                :param inputLength: length of input vectors
                :type inputLength: int
                :param outputLength: length of output vectors
                :type outputLength: int
                :param weights: list of weights for all layers in neural network
                :type weights: List<String>
                :param trainingModel: processed and ready training model for neural network
                :type trainingModel: Map<String, Double>

        .. py:method:: private String accuracy()

        	Calculate neural network accuracy for its training data model.

        	:returns: formatted accuracy print
        	:rtype: String

	.. py:method:: public List<String> getAllWeights()

		Collect neurons weights from all layers.

                :returns: weights of all layers
                :rtype: List<String>

	.. py:method:: public Layer[] getLayers()

		Get access to neural network layers.

                :returns: neural network layers
                :rtype: Layer[]

	.. py:method:: public Map<String, Double> getTrainingModel()

		Get access to neural network training data model.

                :returns: training data model
                :rtype: Map<String, Double>

	.. py:method:: public boolean isLearning()

		Check if neural network is learning at request time.

                :returns: logical value of neural network learning status
                :rtype: boolean

	.. py:method:: private double networkError()

		Calculate whole neural network error.

                :returns: neural network error
                :rtype: double

	.. py:method:: public String predict(String word)

		Predict class of given word.

                :param word: word to classify by neural network
                :type word: String
                :returns: word class
                :rtype: String

	.. py:method:: public void appendToTrainingModel(Map<String, Double> queue)

		Append new data to training data model.

		:param queue: list of new training examples
		:type queue: Map<String, Double>

	.. py:method:: private void createLayers(int inputLength, int outputLength)

		Create layers of neural network (two hidden and output one).

                :param inputLength: length of input vectors
                :type inputLength: int
                :param outputLength: length of output vectors
                :type outputLength: int

	.. py:method:: public void learn(String chartsPath, String accuracyPath)

		Learn neural network to classify words (negative or positive).

		:param chartsPath: path to generated charts folder
		:type path: String
		:param accuracyPath: path to folder with accuracy prints
		:type accuracyPath: String

	.. py:method:: private void setWeights()

		Set weights loaded from collection for all neurons in all layers.

=========
Utilities
=========

.. py:class:: Utilities

	Utilities and tools for neural network.

	.. py:method:: public static double[] addVectors(double[] firstVector, double[] secondVector)

		Add two vectors with equal length.

                :param firstVector: first vector to create sum
                :type firstVector: double[]
                :param secondVector: second vector to create sum
                :type secondVector: double[]
                :returns: sum of two vectors
                :rtype: double[]

	.. py:method:: public static double[] encodeWord(String word)

		Encode given word to vector representation.

                :param word: word to encode
                :type word: String
                :returns: encoded word as vector
                :rtype: double[]

	.. py:method:: public static Map<String, Double> loadTrainingModel(String path)

		Load training data model from file.

                :param path: path to training data model file
                :type path: String
                :returns: list of words with their classification label
                :rtype: Map<String, Double>

	.. py:method:: public static List<String> loadWeights(String path)

		Load neural network weights for all layers from file.

                :param path: path to weights file
                :type path: String
                :return: list of weights for all layers in neural network
                :rtype: List<String>

	.. py:method:: public static double[] multiplyVector(double[] vector, double constant)

		Multiply vector by constant value.

                :param vector: vector to multiply
                :type vector: double[]
                :param constant: constant value
                :type constant: double
                :returns: vector multiplied by constant value
                :rtype: double[]

	.. py:method:: public static double multiplyVectors(double[] firstVector, double[] secondVector)

		Multiply two vectors with equal length.

                :param firstVector: first vector to multiply
                :type firstVector: double[]
                :param secondVector: second vector to multiply
                :type secondVector: double[]
                :returns: multiplication of two vectors
                :rtype: double

        .. py:method:: public static void generateChart(XYSeries chartData, String path)

		Generate chart from neural network learning process.

		:param chartData: coordinates from neural network learning process (epochs and network error)
		:type chartData: XYSeries
 		:param path: path to chart saving location
 		:type path: String

 	.. py:method:: public static void saveAccuracy(String print, String path)

 		Save neural network accuracy print to file.

 		:param print: neural network accuracy print
 		:type print: String
                :param path: path to save location (folder)
                :type path: String

	.. py:method:: public static void saveTrainingModel(Map<String, Double> trainingModel, String path)

		Save training data model to file.

                :param trainingModel: training data model from neural network
                :type trainingModel: Map<String, Double>
                :param path: path to save location (folder)
                :type path: String

	.. py:method:: public static void saveWeights(List<String> weights, String path)

		Save neural network weights from all layers to file for future usage.

                :param weights: weights from all layers of neural network
                :type weights: List<String>
                :param path: path to save location (folder)
                :type path: String