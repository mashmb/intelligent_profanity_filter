Package bgm.nai.server.components
=================================

Package responsible for group chat server graphical user interface components.

.. image:: ../../../UML/server_components_package.png
	:align: center

======
Window
======

.. py:class:: class Window extends JFrame

	Main window for chat server.

	.. py:method:: public Window()

		Window constructor and settings.

=================
AutoScrollTextBox
=================

.. py:class:: class AutoScrollTextBox extends JScrollPane

	Scroll pane with text box with auto scroll feature.

	.. py:method:: public AutoScrollTextBox()

		AutoScrollTextBox constructor and components settings.

	.. py:method:: public JTextArea getTextarea()

		Get access to text area inside scroll pane.

                :returns: text area inside scroll pane
                :rtype: JTextArea