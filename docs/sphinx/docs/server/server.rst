Package bgm.nai.server
======================

Package responsible for group chat server.

.. image:: ../../../UML/server_package.png
	:align: center

==========
ChatServer
==========

.. py:class:: ChatServer

	Main entry point for group chat server.

	.. py:method:: public static void main(String[] args)

		Initialize group chat server program.

                :param args: program arguments
                :type args: String[]

=====
Model
=====

.. py:class:: Model

	Data layer for group chat server.

	.. py:method:: public String getAccuracyPath()

		Get access to saved neural network accuracy prints location (folder).

		:returns: saved neural network accuracy prints path
		:rtype: String

	.. py:method:: public NeuralNetwork getAdditionalFFN()

		Get access to additional neural network.

                :returns: additional neural network
                :rtype: NeuralNetwork

        .. py:method:: public String getChartsPath()

        	Get access to localization with generated charts.

        	:returns: path to generated charts folder
        	:rtype: String

	.. py:method:: public Map<String, ChatClientInt> getClients()

		Get access to registered clients list.

                :returns: list of registered clients
                :rtype: Map<String, ChatClientInt>

	.. py:method:: public String getLocalhost()

		Get access to group chat server IP address.

                :returns: group chat server IP address
                :rtype: String

	.. py:method:: public NeuralNetwork getMainFFN()

		Get access to main neural network.

                :returns: main neural network
                :rtype: NeuralNetwork

	.. py:method:: public boolean getNeedChangeFFN()

		Get access to status of need to change neural network.

                :returns: need change status
                :rtype: boolean

	.. py:method:: public int getPort()

		Get access to group chat server port.

                :returns: group chat server port
                :rtype: int

	.. py:method:: public String getTrainingModelsPath()

		Get access to saved training models location (folder).

                :returns: saved training models path
                :rtype: String

	.. py:method:: public String getWeightsPath()

		Get access to saved weights location (folder).

                :returns: saved weights path
                :rtype: String

	.. py:method:: public Map<String, Double> getWordsQueue()

		Get access to words queue.

                :returns: words queue
                :rtype: Map<String, Double>

	.. py:method:: public void resetWordsQueue()

		Reset words queue (make it empty).

	.. py:method:: public void setAdditionalFFN(NeuralNetwork nn)

		Set additional neural network.

                :param nn: additional neural network
                :type nn: NeuralNetwork

	.. py:method:: public void setLocalhost(String ip)

		Set IP address of localhost.

                :param ip: IP address
                :type ip: String

	.. py:method:: public void setMainFFN(NeuralNetwork nn)

		Set main neural network.

                :param nn: main neural network
                :type nn: NeuralNetwork

	.. py:method:: public void setNeedChangeFFN(boolean status)

		Change status of need change neural network.

                :param status: status as logical value
                :type status: boolean

====
View
====

.. py:class:: View

	View layer for the group chat server.

	.. py:method:: public View()

		View constructor and components settings.

	.. py:method:: public AutoScrollTextBox getNeuralNetworkBox()

		Get access to text box with neural network learning information.

                :returns: text box with neural network learning information
                :rtype: AutoScrollTextBox

	.. py:method:: public JLabel getIpLabel()

		Get access to label with server IP address.

                :returns: label with server IP address
                :rtype: JLabel

	.. py:method:: public AutoScrollTextBox getMessagesBox()

		Get access to text box with messages that server receives.

                :returns: text box with messages that server receives
                :rtype: AutoScrollTextBox

	.. py:method:: public JButton getStartButton()

		Get access to button that starts chat server.

                :returns: button that starts chat server
                :rtype: JButton

	.. py:method:: public JButton getStopButton()

		Get access to button that stops chat server.

                :returns: button that stops chat server
                :rtype: JButton

	.. py:method:: public Window getWindow()

		Get access to group chat server window.

                :returns: group chat server window
                :rtype: Window

	.. py:method:: private void setUI()

		Set system theme for Java Swing components.

==============
ChatServerImpl
==============

.. py:class:: class ChatServerImpl extends UnicastRemoteObject

	Implementation of group chat server, network layer.

	.. py:method:: protected ChatServerImpl(Model model, View view) throws RemoteException

		ChatServerImpl constructor.

                :param model: access to data layer
                :type model: Model
                :param view: access to view layer
                :type view: View
                :throws RemoteException: exception thrown while problem with network communication occur

	.. py:method:: public boolean connect(ChatClientInt client, String nickname, String ipAddress) throws RemoteException

		Register new client in group chat server database for future data sharing.

                :param client: list of remote methods available on connected client site
                :type client: ChatClientInt
                :param nickname: nickname for which client will be visible during conversation
                :type nickname: String
                :param ipAddress: IP address of connected client
                :type ipAddress: String
                :returns: logical value if client connected successfully
                :rtype: boolean
                :throws RemoteException: exception thrown while problem with network communication occur

	.. py:method:: public boolean disconnect(String nickname, String ipAddress) throws RemoteException

		Unregister already connected client from the group chat server.

                :param nickname: nickname of client that sends request
                :type nickname: String
                :param ipAddress: IP address of client that sends request
                :type ipAddress: String
                :returns: logical value if client disconnected properly
                :rtype: boolean
                :throws RemoteException: exception thrown while problem with network communication occur

	.. py:method:: private void broadcastMessage(String sender, String message)

		Broadcast new message to all clients.

                :param sender: sender nickname
                :type sender: String
                :param message: new message
                :type message: String

	.. py:method:: public void detectInterface()

		Find proper IP address for network communication (proper interface).

	.. py:method:: public void verifyMessage(String message, String nickname, String ipAddress) throws RemoteException

		Use neural network to verify message (profanity filtering) and send message to rest of the clients.

                :param message: message from client
                :type message: String
                :param nickname: nickname of client that sends message
                :type nickname: String
                :param ipAddress: IP address of client that sends message
                :type ipAddress: String
                :throws RemoteException: exception thrown while problem with network communication occur

==========
Controller
==========

.. py:class:: Controller

	Control layer for group chat server.

	.. py:method:: public Controller(Model model, View view, ChatServerImpl networkDriver)

		Controller constructor.

                :param model: access to data layer
                :type model: Model
                :param view: access to view layer
                :type view: View
                :param networkDriver: access to network layer
                :type networkDriver: ChatServerImpl

	.. py:method:: public void init()

		Initialize group chat server program and controls listeners.

	.. py:method:: private void chooseFiles()

		Choose training data model file and weights file, initialize main feed forward neural network.

	.. py:method:: private void start()

		Start group chat server (RMI protocol).

	.. py:method:: private void stop()

		Stop group chat server (RMI protocol).