Package bgm.nai.shared.interfaces
=================================

Package responsible for server and client interfaces that ensures RMI protocol connection.

.. image:: ../../../UML/shared_interfaces_package.png
	:align: center

==============
ChatServerInt
==============

.. py:class:: interface ChatServerInt extends Remote

	Definition of methods that group chat server provides for remote communication.

	.. py:method:: boolean connect(ChatClientInt client, String nickname, String ipAddress) throws RemoteException

		Register new client in group chat server database for future data sharing.

                :param client: list of remote methods available on connected client site
                :type client: ChatClientInt
                :param nickname: nickname for which client will be visible during conversation
                :type nickname: String
                :param ipAddress: IP address of connected client
                :type ipAddress: String
                :returns: logical value if client connected successfully
                :rtype: boolean
                :throws RemoteException: exception thrown while problem with network communication occur

	.. py:method:: boolean disconnect(String nickname, String ipAddress) throws RemoteException

		Unregister already connected client from the group chat server.

                 :param nickname: nickname of client that sends request
                 :type nickname: String
                 :param ipAddress: IP address of client that sends request
                 :type ipAddress: String
                 :returns: logical value if client disconnected properly
                 :rtype: boolean
                 :throws RemoteException: exception thrown while problem with network communication occur

	.. py:method:: void verifyMessage(String message, String nickname, String ipAddress) throws RemoteException

		Use neural network to verify message (profanity filtering) and send message to rest of the clients.

                 :param message: message from client
                 :type message: String
                 :param nickname: nickname of client that sends message
                 :type nickname: String
                 :param ipAddress: IP address of client that sends message
                 :type ipAddress: String
                 :throws RemoteException: exception thrown while problem with network communication occur

=============
ChatClientInt
=============

.. py:class:: interface ChatClientInt extends Remote

	Definition of methods that group chat client provides for remote communication.

	.. py:method:: public void updateMessageBox(String message) throws RemoteException

		Update message box with new message.

		:param message: new message
		:type message: String
		:throws RemoteException: exception thrown while problem with network communication occur