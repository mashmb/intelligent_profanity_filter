package bgm.nai.client;

import bgm.nai.logger.CustomLogger;
import java.rmi.RemoteException;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;

/**
 * Group chat client main entry point.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class ChatClient {

	private static CustomLogger cl = new CustomLogger(ChatClient.class.getName());
	private static Logger logger = cl.getLogger();

	/**
	 * Initialize group chat client program.
	 *
	 * @param args program arguments
	 */
	public static void main(String[] args) {

		SwingUtilities.invokeLater(() -> {

			try {
				Model model = new Model();
				View view = new View();
				ChatClientImpl networkDriver = new ChatClientImpl(model, view);
				Controller controller = new Controller(model, view, networkDriver);
				controller.init();
			} catch (RemoteException ex) {
				logger.severe("Cannot launch group chat client");
				ex.printStackTrace();
			}

		});

	}

}
