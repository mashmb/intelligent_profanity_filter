package bgm.nai.client;

import bgm.nai.logger.CustomLogger;
import bgm.nai.shared.interfaces.ChatClientInt;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implementation of group chat client, network layer.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class ChatClientImpl extends UnicastRemoteObject
	implements ChatClientInt {

	private static CustomLogger cl = new CustomLogger(ChatClientImpl.class.getName());
	private static Logger logger = cl.getLogger();

	private Model model;
	private View view;

	/**
	 * ChatClientImpl constructor.
	 *
	 * @param model access to data layer
	 * @param view  access to view layer
	 * @throws RemoteException exception thrown while problem
	 *                         with network communication occur
	 */
	protected ChatClientImpl(Model model, View view) throws RemoteException {
		this.model = model;
		this.view = view;
	}

	/**
	 * Find proper IP address for network communication (proper
	 * interface).
	 */
	public void detectInterface() {
		logger.info("Trying to get IP address for network communication...");

		try {
			Socket validator = new Socket();
			validator.connect(new InetSocketAddress("google.com", 80));
			String ip = validator.getLocalAddress().getHostAddress();
			validator.close();
			logger.log(Level.INFO, "Proper network interface detected: {0}", ip);
			model.setLocalhost(ip);
		} catch (IOException ex) {
			logger.severe("There are no network interfaces that meet the requirements");
			ex.printStackTrace();
		}
	}

	/**
	 * Update message box with new message.
	 *
	 * @param message new message
	 * @throws RemoteException exception thrown while problem
	 *                         with network communication occur
	 */
	@Override
	public void updateMessageBox(String message) throws RemoteException {
		view.getMessagesScrollPane().getTextarea().append(message + "\n");
	}

}
