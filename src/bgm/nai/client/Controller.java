package bgm.nai.client;

import bgm.nai.client.components.InitialPane;
import bgm.nai.logger.CustomLogger;
import bgm.nai.shared.interfaces.ChatServerInt;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * Control layer for group chat client.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class Controller {

	private static CustomLogger cl = new CustomLogger(Controller.class.getName());
	private static Logger logger = cl.getLogger();

	private Model model;
	private View view;
	private ChatClientImpl networkDriver;

	private InitialPane initialPane = new InitialPane();

	/**
	 * Controller constructor.
	 *
	 * @param model         access to data layer
	 * @param view          access to view layer
	 * @param networkDriver access to network layer
	 */
	public Controller(Model model, View view, ChatClientImpl networkDriver) {
		this.model = model;
		this.view = view;
		this.networkDriver = networkDriver;
	}

	/**
	 * Initialize group chat client program and controls listeners.
	 */
	public void init() {
		view.getWindow().addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent windowEvent) {
				disconnect();
			}

		});

		ActionListener buttonListener = ActionEvent -> {
			sendMessage(view.getMessageScrollPane().getTextarea().getText());
			view.getMessageScrollPane().getTextarea().setText("");
		};
		view.getSendButton().addActionListener(buttonListener);

		networkDriver.detectInterface();
		connect();
	}

	/**
	 * Connect to group chat server. Before connection initialization
	 * server IP address must be validated. If everything goes properly
	 * user will bw connected to group chat server. Nickname for every
	 * client must be unique.
	 */
	private void connect() {
		logger.info("Trying to connect to group chat server...");
		int result = JOptionPane.showConfirmDialog(view.getWindow(), initialPane,
			"Configuration", JOptionPane.OK_CANCEL_OPTION);

		if (result == JOptionPane.OK_OPTION) {
			String serverIp = initialPane.getServerIpField().getText();

			if (model.validateIp(serverIp)) {
				model.setServerIp(serverIp);
				String nickname = initialPane.getNicknameField().getText().trim()
					.replaceAll("\\s+", "").toLowerCase();

				try {
					Registry registry = LocateRegistry.getRegistry(model.getServerIp(), model.getServerPort());
					model.setRemoteServer((ChatServerInt) registry.lookup("rmi://" + model.getServerIp() + ":" + model.getServerPort() + "/groupChat"));

					if (model.getRemoteServer().connect(networkDriver, nickname, model.getLocalhost())) {
						model.setNickname(nickname);
						view.getNickLabel().setText(model.getNickname() + "@" + model.getLocalhost());
						logger.log(Level.INFO, "Connected to group chat server {0} as {1}",
							new Object[]{model.getServerIp(), nickname});
					} else {
						logger.log(Level.WARNING, "Nickname {0} already exists on server", nickname);
						JOptionPane.showMessageDialog(view.getWindow(), "Configuration",
							"Chosen nickname already exists on server", JOptionPane.WARNING_MESSAGE);
						connect();
					}
				} catch (Exception ex) {
					logger.log(Level.SEVERE, "Cannot connect to group chat server: {0}:{1}",
						new Object[]{model.getServerIp(), model.getServerPort()});
					ex.printStackTrace();
					JOptionPane.showMessageDialog(view.getWindow(), "Cannot connect to group chat server, try again later",
						"Configuration", JOptionPane.ERROR_MESSAGE);
					forceExit();
				}

			} else {
				JOptionPane.showMessageDialog(view.getWindow(), "Not valid IP address",
					"Error", JOptionPane.ERROR_MESSAGE);
				connect();
			}
		} else {
			forceExit();
		}
	}

	/**
	 * Disconnect from group chat server. Disconnection should be
	 * accepted by server.
	 */
	private void disconnect() {
		logger.info("Trying to disconnect from group chat server...");

		try {

			if (model.getRemoteServer().disconnect(model.getNickname(), model.getLocalhost())) {
				logger.info("Disconnected successfully");
			} else {
				JOptionPane.showMessageDialog(view.getWindow(), "Cannot disconnect safety, trying to force exit...",
					"Error", JOptionPane.ERROR_MESSAGE);
				logger.severe("Cannot disconnect safety, trying to force exit...");
			}

		} catch (Exception ex) {
			logger.severe("Connection problem with group chat server occurred, force exiting...");
			ex.printStackTrace();
		}
	}

	/**
	 * Safety force group chat client program to exit.
	 */
	private void forceExit() {
		view.getWindow().dispatchEvent(new WindowEvent(view.getWindow(), WindowEvent.WINDOW_CLOSING));
	}

	/**
	 * Send new message to group chat server.
	 *
	 * @param message message to send
	 */
	private void sendMessage(String message) {
		logger.info("Trying to send new message...");

		if (!message.equals("")) {
			message = message.trim();

			try {
				model.getRemoteServer().verifyMessage(message, model.getNickname(), model.getServerIp());
				logger.info("Message sent");
			} catch (RemoteException ex) {
				logger.severe("Problem with connection occurred while sending message");
				ex.printStackTrace();
			}
		} else {
			logger.warning("Cannot send empty message");
		}
	}

}
