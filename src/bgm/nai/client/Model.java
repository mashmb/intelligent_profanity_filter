package bgm.nai.client;

import bgm.nai.shared.interfaces.ChatServerInt;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Data layer for group chat client.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class Model {

	private String localhost = null;
	private String serverIp = null;
	private ChatServerInt remoteServer;
	private String nickname = null;

	/**
	 * Get access to localhost IP address.
	 *
	 * @return String localhost IP address
	 */
	public String getLocalhost() {
		return localhost;
	}

	/**
	 * Get access to group chat client nickname.
	 *
	 * @return String group chat client nickname
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * Get access to remote group chat server object.
	 *
	 * @return ChatServerInt remote group chat server object
	 */
	public ChatServerInt getRemoteServer() {
		return remoteServer;
	}

	/**
	 * Get access to group chat server IP address.
	 *
	 * @return String group chat server IP address
	 */
	public String getServerIp() {
		return serverIp;
	}

	/**
	 * Get access to group chat server port.
	 *
	 * @return int group chat server port
	 */
	public int getServerPort() {
		return 50000;
	}

	/**
	 * Check if given IP address is correct.
	 *
	 * @param ip IP address to validate
	 * @return boolean logical value if IP address is valid
	 */
	public boolean validateIp(String ip) {
		String ipForm = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
		Pattern ipPattern = Pattern.compile(ipForm);
		Matcher ipMatcher = ipPattern.matcher(ip);

		return ipMatcher.matches();
	}

	/**
	 * Set IP address of group chat client.
	 *
	 * @param localhost IP address
	 */
	public void setLocalhost(String localhost) {
		this.localhost = localhost;
	}

	/**
	 * Set nickname for user that will be chatting.
	 *
	 * @param nick chosen nickname
	 */
	public void setNickname(String nick) {
		nickname = nick;
	}

	/**
	 * Set remote group chat server object.
	 *
	 * @param serverInterface remote group chat server object
	 */
	public void setRemoteServer(ChatServerInt serverInterface) {
		remoteServer = serverInterface;
	}

	/**
	 * Set IP address of group chat server.
	 *
	 * @param ip IP address
	 */
	public void setServerIp(String ip) {
		serverIp = ip;
	}

}
