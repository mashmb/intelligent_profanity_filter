package bgm.nai.client;

import bgm.nai.client.components.MessageScrollPane;
import bgm.nai.client.components.MessagesScrollPane;
import bgm.nai.client.components.Window;
import bgm.nai.logger.CustomLogger;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.UIManager;

/**
 * View layer for chat client.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class View {

	private static CustomLogger cl = new CustomLogger(View.class.getName());
	private static Logger logger = cl.getLogger();

	private Window window;

	private MessagesScrollPane messagesScrollPane;
	private MessageScrollPane messageScrollPane;

	private JButton sendButton;

	private JLabel nickLabel;

	/**
	 * View constructor and components settings.
	 */
	public View() {
		setUI();

		this.window = new Window();
		this.messagesScrollPane = new MessagesScrollPane();
		this.messageScrollPane = new MessageScrollPane();
		this.sendButton = new JButton("SEND");
		this.nickLabel = new JLabel();
		JLabel messageLabel = new JLabel("Message");

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;

		// nickLabel settings
		this.nickLabel.setPreferredSize(new Dimension(600, 30));
		this.nickLabel.setFont(new Font("Arial", Font.PLAIN, 16));
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 60;
		gbc.gridheight = 3;
		this.window.add(this.nickLabel, gbc);

		// messagesScrollPane settings
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 60;
		gbc.gridheight = 34;
		this.window.add(this.messagesScrollPane, gbc);

		// messageLabel settings
		messageLabel.setPreferredSize(new Dimension(600, 30));
		messageLabel.setFont(new Font("Arial", Font.PLAIN, 16));
		gbc.gridx = 0;
		gbc.gridy = 37;
		gbc.gridwidth = 60;
		gbc.gridheight = 3;
		this.window.add(messageLabel, gbc);

		// messageScrollPane settings
		gbc.gridx = 0;
		gbc.gridy = 40;
		gbc.gridwidth = 40;
		gbc.gridheight = 6;
		this.window.add(this.messageScrollPane, gbc);

		// sendButton settings
		this.sendButton.setPreferredSize(new Dimension(200, 60));
		this.sendButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
		this.sendButton.setToolTipText("Send message");
		this.sendButton.setFont(new Font("Arial", Font.PLAIN, 16));
		gbc.gridx = 40;
		gbc.gridy = 40;
		gbc.gridwidth = 20;
		gbc.gridheight = 6;
		this.window.add(this.sendButton, gbc);
	}

	/**
	 * Get access to scroll pane with text area where new message is
	 * created.
	 *
	 * @return MessageScrollPane scroll pane with text area where
	 * new message is created
	 */
	public MessageScrollPane getMessageScrollPane() {
		return messageScrollPane;
	}

	/**
	 * Get access to scroll pane with text area that contains conversation.
	 *
	 * @return MessagesScrollPane scroll pane with text area that
	 * contains conversation
	 */
	public MessagesScrollPane getMessagesScrollPane() {
		return messagesScrollPane;
	}

	/**
	 * Get access to label with nickname.
	 *
	 * @return JLabel label with nickname
	 */
	public JLabel getNickLabel() {
		return nickLabel;
	}

	/**
	 * Get access to send button.
	 *
	 * @return JButton send button
	 */
	public JButton getSendButton() {
		return sendButton;
	}

	/**
	 * Get access to group chat client window.
	 *
	 * @return Window group chat client window
	 */
	public Window getWindow() {
		return window;
	}

	/**
	 * Set system theme for Java Swing components.
	 */
	private void setUI() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception ex) {
			logger.severe("Cannot set system theme  for Java Swing components");
			ex.printStackTrace();
		}
	}

}
