package bgm.nai.client.components;

import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Initial pane to get group chat server IP address and nickname
 * from user.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class InitialPane extends JPanel {

	private JTextField serverIpField = new JTextField(10);
	private JTextField nicknameField = new JTextField(10);

	/**
	 * InitialPane constructor and components settings.
	 */
	public InitialPane() {
		// serverIpLabel settings
		JLabel serverIpLabel = new JLabel("Server IP:");
		serverIpLabel.setFont(new Font("Arial", Font.PLAIN, 16));

		// serverIpField settings
		this.serverIpField.setFont(new Font("Arial", Font.PLAIN, 16));

		// nicknameLabel settings
		JLabel nicknameLabel = new JLabel("Nickname:");
		nicknameLabel.setFont(new Font("Arial", Font.PLAIN, 16));

		// nicknameField settings
		this.nicknameField.setFont(new Font("Arial", Font.PLAIN, 16));

		// InitialPane settings
		this.add(serverIpLabel);
		this.add(this.serverIpField);
		this.add(nicknameLabel);
		this.add(this.nicknameField);
	}

	/**
	 * Get access to field where user types their nickname for
	 * conversation.
	 *
	 * @return JTextField field with user nickname
	 */
	public JTextField getNicknameField() {
		return nicknameField;
	}

	/**
	 * Get access to field where user types group chat server IP
	 * address.
	 *
	 * @return JTextField field with group chat server IP address
	 */
	public JTextField getServerIpField() {
		return serverIpField;
	}

}
