package bgm.nai.client.components;

import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Scroll pane with text area that contains message to send to the
 * chat server.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class MessageScrollPane extends JScrollPane {

	private JTextArea textarea = new JTextArea();

	/**
	 * MessageScrollPane constructor and components settings.
	 */
	public MessageScrollPane() {
		// textarea settings
		this.textarea.setLineWrap(true);
		this.textarea.setWrapStyleWord(true);
		this.textarea.setToolTipText("Write message");
		this.textarea.setFont(new Font("Arial", Font.PLAIN, 12));

		// MessageScrollPane settings
		this.setPreferredSize(new Dimension(400, 60));
		this.setViewportView(this.textarea);
	}

	/**
	 * Get access to text area with new message.
	 *
	 * @return JTextArea text area with new message
	 */
	public JTextArea getTextarea() {
		return textarea;
	}

}
