package bgm.nai.client.components;

import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

/**
 * Scroll pane with auto scrolling text with messages received
 * from server.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class MessagesScrollPane extends JScrollPane {

	private JTextArea textarea = new JTextArea();

	/**
	 * MessagesScrollPane constructor and components settings.
	 */
	public MessagesScrollPane() {
		// textarea settings
		this.textarea.setEditable(false);
		this.textarea.setLineWrap(true);
		this.textarea.setWrapStyleWord(true);
		this.textarea.setFont(new Font("Arial", Font.PLAIN, 12));
		DefaultCaret caret = (DefaultCaret) this.textarea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		// MessagesScrollPane settings
		this.setPreferredSize(new Dimension(600, 300));
		this.setViewportView(this.textarea);
	}

	/**
	 * Get access to text area with conversation.
	 *
	 * @return JTextArea text area with conversation
	 */
	public JTextArea getTextarea() {
		return textarea;
	}

}
