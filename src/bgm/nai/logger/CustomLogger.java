package bgm.nai.logger;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Customized logger with console handler and custom formatter.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class CustomLogger {

	private String className;
	private LogFormat formatter = new LogFormat();
	private ConsoleHandler handler = new ConsoleHandler();

	/**
	 * CustomLogger constructor.
	 *
	 * @param className name of the class that uses logger
	 */
	public CustomLogger(String className) {
		this.className = className;
	}

	/**
	 * Get access to customized logger (custom console handler and
	 * custom messages appearance).
	 *
	 * @return Logger customized logger
	 */
	public Logger getLogger() {
		Logger logger = Logger.getLogger(className);
		logger.setUseParentHandlers(false);
		logger.setLevel(Level.ALL);
		handler.setLevel(Level.ALL);
		handler.setFormatter(formatter);
		logger.addHandler(handler);

		return logger;
	}

}
