package bgm.nai.logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * Custom logger messages appearance.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class LogFormat extends Formatter {

	private DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

	/**
	 * Format logger record to customized message.
	 *
	 * @param logRecord logger record
	 * @return String formatted logger message
	 */
	@Override
	public String format(LogRecord logRecord) {
		StringBuilder sb = new StringBuilder();
		sb.append(dateFormat.format(new Date(logRecord.getMillis())));
		sb.append(" - ");
		sb.append("(").append(logRecord.getLoggerName()).append(")");
		sb.append(" ");
		sb.append("[").append(logRecord.getLevel()).append("]");
		sb.append(":").append(" ");
		sb.append(formatMessage(logRecord));
		sb.append("\n");

		return sb.toString();
	}

}
