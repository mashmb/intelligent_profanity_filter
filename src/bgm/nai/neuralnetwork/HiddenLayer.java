package bgm.nai.neuralnetwork;

import bgm.nai.logger.CustomLogger;
import java.util.logging.Logger;

/**
 * Hidden layer for multilayer neural network.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class HiddenLayer extends Layer {

	private static CustomLogger cl = new CustomLogger(HiddenLayer.class.getName());
	private static Logger logger = cl.getLogger();

	/**
	 * HiddenLayer constructor.
	 *
	 * @param numberOfNeurons number of neurons in hidden
	 *                        layer
	 * @param numberOfWeights number of weights for neurons
	 *                        in hidden layer
	 */
	public HiddenLayer(int numberOfNeurons, int numberOfWeights) {
		super(numberOfNeurons, numberOfWeights);
	}

	/**
	 * Calculate error for each neuron in output layer.
	 *
	 * @param answer proper answer for given training example
	 *               as vector
	 */
	@Override
	public void neuronsError(double[] answer) {
	}

	/**
	 * Calculate error for each neuron in hidden layer.
	 *
	 * @param nextLayer next layer in multilayer neural network
	 */
	@Override
	public void neuronsError(Neuron[] nextLayer) {
		logger.info("Calculating errors in hidden layer...");

		for (int i = 0; i < super.neurons.length; i++) {
			double series = 0;

			for (Neuron neuron : nextLayer) {
				series += neuron.getError() * neuron.getWeights()[i];
			}

			double derivative = 0.5 * (1 - Math.pow(super.output[i], 2));
			super.neurons[i].setError(derivative * series);
		}

		logger.info("Errors in hidden layer calculated");
	}

}
