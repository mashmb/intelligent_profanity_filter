package bgm.nai.neuralnetwork;

import bgm.nai.logger.CustomLogger;
import java.util.logging.Logger;

/**
 * Standard layer construction for multilayer neural network.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public abstract class Layer {

	private static CustomLogger cl = new CustomLogger(Layer.class.getName());
	private static Logger logger = cl.getLogger();

	public Neuron[] neurons;
	public double[] output;

	/**
	 * Layer constructor.
	 *
	 * @param numberOfNeurons number of neurons in layer
	 * @param numberOfWeights number of weights for neurons
	 *                        in layer
	 */
	public Layer(int numberOfNeurons, int numberOfWeights) {
		this.neurons = new Neuron[numberOfNeurons];
		this.output = new double[numberOfNeurons];
		initialize(numberOfWeights);
	}

	/**
	 * Calculate output values from neurons in layer.
	 *
	 * @param inputVector input vector for layer
	 */
	public void calculateLayerOutput(double[] inputVector) {
		logger.info("Collecting output signals from layer...");

		for (int i = 0; i < neurons.length; i++) {
			output[i] = neurons[i].outputSignal(inputVector);
		}

		logger.info("Output signals from layer collected");
	}

	/**
	 * Initialize layer by creating proper number of neurons in layer.
	 *
	 * @param numberOfWeights number of weights for neurons
	 *                        in layer
	 */
	private void initialize(int numberOfWeights) {
		logger.info("Initializing layer...");

		for (int i = 0; i < neurons.length; i++) {
			neurons[i] = new Neuron(numberOfWeights);
		}

		logger.info("Layer initialized");
	}

	/**
	 * Calculate error for each neuron in output layer.
	 *
	 * @param answer proper answer for given training example
	 *               as vector
	 */
	public abstract void neuronsError(double[] answer);

	/**
	 * Calculate error for each neuron in layer (hidden layer usage).
	 *
	 * @param nextLayer next layer in multilayer neural network
	 */
	public abstract void neuronsError(Neuron[] nextLayer);

	/**
	 * Update weights for all neurons in layer.
	 *
	 * @param learningRate neural network learning rate
	 * @param inputVector  input vector for layer
	 */
	public void updateWeights(double learningRate, double[] inputVector) {
		logger.info("Updating weights in layer neurons...");

		for (Neuron neuron : neurons) {
			neuron.updateWeights(learningRate, inputVector);
		}

		logger.info("Weights updated in layer neurons");
	}

}
