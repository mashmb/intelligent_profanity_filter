package bgm.nai.neuralnetwork;

import bgm.nai.logger.CustomLogger;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.data.xy.XYSeries;

/**
 * Neural network representation.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class NeuralNetwork {

	private static CustomLogger cl = new CustomLogger(NeuralNetwork.class.getName());
	private static Logger logger = cl.getLogger();

	private Layer[] layers;
	private List<String> weights;
	private Map<String, Double> trainingModel;
	private boolean learningStatus = false;

	/**
	 * NeuralNetwork constructor.
	 *
	 * @param inputLength   length of input vectors
	 * @param outputLength  length of output vectors
	 * @param weights       list of weights for all layers in neural
	 *                      network
	 * @param trainingModel processed and ready training model
	 *                      for neural network
	 */
	public NeuralNetwork(int inputLength, int outputLength, List<String> weights, Map<String, Double> trainingModel) {
		this.weights = weights;
		this.trainingModel = trainingModel;
		createLayers(inputLength, outputLength);
	}

	/**
	 * Calculate neural network accuracy for its training data
	 * model.
	 *
	 * @return String formatted accuracy print
	 */
	private String accuracy() {
		logger.info("Calculating neural network accuracy...");
		StringBuilder print = new StringBuilder();
		print.append("Accuracy for ").append(trainingModel.size()).append(" training examples").append("\n");
		int counter = 0;

		for (Map.Entry<String, Double> trainingExample : trainingModel.entrySet()) {
			String goodAnswer;

			if (trainingExample.getValue() == 1) {
				goodAnswer = "positive";
			} else {
				goodAnswer = "negative";
			}

			String networkAnswer = this.predict(trainingExample.getKey());

			if (goodAnswer.equals(networkAnswer)) {
				counter++;
			}
		}

		double accuracy = (counter * 100) / trainingModel.size();
		print.append("Correct predictions: ").append(counter).append("/").append(trainingModel.size()).append("\n");
		print.append("Incorrect predictions: ").append(trainingModel.size() - counter).append("/")
			.append(trainingModel.size()).append("\n");
		print.append("Neural network accuracy: ").append(accuracy).append("%");
		logger.info("Accuracy calculated");

		return print.toString();
	}

	/**
	 * Collect neurons weights from all layers.
	 *
	 * @return String weights of all layers
	 */
	public List<String> getAllWeights() {
		logger.info("Collecting weights from all layers...");
		List<String> allWeights = new LinkedList<>();

		for (Layer layer : layers) {
			for (Neuron neuron : layer.neurons) {
				StringBuilder sb = new StringBuilder();

				for (Double weight : neuron.getWeights()) {
					sb.append(weight).append(" ");
				}

				sb.append("\n");
				allWeights.add(sb.toString());
			}
		}

		logger.info("Weights collected");

		return allWeights;
	}

	/**
	 * Get access to neural network layers.
	 *
	 * @return Layer[] neural network layers
	 */
	public Layer[] getLayers() {
		return layers;
	}

	/**
	 * Get access to neural network training data model.
	 *
	 * @return Map training data model
	 */
	public Map<String, Double> getTrainingModel() {
		return trainingModel;
	}

	/**
	 * Check if neural network is learning at request time.
	 *
	 * @return boolean logical value of neural network learning
	 * status
	 */
	public boolean isLearning() {
		return learningStatus;
	}

	/**
	 * Calculate whole neural network error.
	 *
	 * @return double neural network error
	 */
	private double networkError() {
		logger.info("Calculating network error...");
		double error = 0;

		for (Map.Entry<String, Double> trainingRow : trainingModel.entrySet()) {
			layers[0].calculateLayerOutput(Utilities.encodeWord(trainingRow.getKey()));
			layers[1].calculateLayerOutput(layers[0].output);
			layers[2].calculateLayerOutput(layers[1].output);
			double answer = layers[2].output[0];

			if (answer >= -1 && answer <= -0.5) {
				answer = -1;
			} else if (answer >= 0.5 && answer <= 1) {
				answer = 1;
			}

			error += Math.pow((trainingRow.getValue() - answer), 2);
		}

		logger.log(Level.INFO, "Network error calculated: {0}", error);

		return error;
	}

	/**
	 * Predict class of given word.
	 *
	 * @param word word to classify by neural network
	 * @return String word class
	 */
	public String predict(String word) {
		logger.log(Level.INFO, "Predicting class for word: {0}...", word);
		layers[0].calculateLayerOutput(Utilities.encodeWord(word));
		layers[1].calculateLayerOutput(layers[0].output);
		layers[2].calculateLayerOutput(layers[1].output);
		String answer = null;

		if (layers[2].output[0] >= -1 && layers[2].output[0] <= -0.5) {
			answer = "negative";
		} else if (layers[2].output[0] > -0.5 && layers[2].output[0] < 0.5) {
			answer = "unknown";
		} else if (layers[2].output[0] >= 0.5 && layers[2].output[0] <= 1) {
			answer = "positive";
		}

		logger.log(Level.INFO, "Class predicted for word {0}: {1}", new Object[]{word, answer});

		return answer;
	}

	/**
	 * Append new data to training data model.
	 *
	 * @param queue list of new training examples
	 */
	public void appendToTrainingModel(Map<String, Double> queue) {
		for (Map.Entry<String, Double> row : queue.entrySet()) {
			if (!trainingModel.containsKey(row.getKey().toLowerCase())) {
				trainingModel.put(row.getKey().toLowerCase(), row.getValue());
			}
		}
	}

	/**
	 * Create layers of neural network (two hidden and output one).
	 *
	 * @param inputLength  length of input vectors
	 * @param outputLength length of output vectors
	 */
	private void createLayers(int inputLength, int outputLength) {
		logger.info("Creating neural network...");
		layers = new Layer[3];
		int inputLayerLength = (inputLength + outputLength) / 2;
		int hiddenLayerLength = (int) Math.sqrt(inputLayerLength * outputLength);
		logger.log(Level.INFO, "Neural network dimensions [neurons, inputs]: {0} x {1}, {2} x {3}, {4} x {5}",
			new Object[]{inputLayerLength, inputLength, hiddenLayerLength, inputLayerLength,
				outputLength, hiddenLayerLength});
		layers[0] = new HiddenLayer(inputLayerLength, inputLength);
		layers[1] = new HiddenLayer(hiddenLayerLength, inputLayerLength);
		layers[2] = new OutputLayer(outputLength, hiddenLayerLength);
		setWeights();
		logger.info("Neural network created");
	}

	/**
	 * Learn neural network to classify words (negative or positive).
	 *
	 * @param chartsPath   path to generated charts folder
	 * @param accuracyPath path to folder with accuracy prints
	 */
	public void learn(String chartsPath, String accuracyPath) {
		if (trainingModel != null && !trainingModel.isEmpty()) {
			logger.info("Learning...");
			XYSeries chartData = new XYSeries("Network error");
			learningStatus = true;
			double error = networkError();
			double learningRate = 0.1;
			int epoch = 1;

			while (error > 0 && epoch <= 500) {
				logger.log(Level.INFO, "Epoch: {0}", epoch);

				for (Map.Entry<String, Double> trainingRow : trainingModel.entrySet()) {
					double[] inputVector = Utilities.encodeWord(trainingRow.getKey());
					layers[0].calculateLayerOutput(inputVector);
					layers[1].calculateLayerOutput(layers[0].output);
					layers[2].calculateLayerOutput(layers[1].output);
					layers[2].neuronsError(new double[]{trainingRow.getValue()});
					layers[1].neuronsError(layers[2].neurons);
					layers[0].neuronsError(layers[1].neurons);
					layers[2].updateWeights(learningRate, layers[1].output);
					layers[1].updateWeights(learningRate, layers[0].output);
					layers[0].updateWeights(learningRate, inputVector);
				}

				error = networkError();
				chartData.add(epoch, error);
				epoch += 1;
			}

			learningStatus = false;
			Utilities.generateChart(chartData, chartsPath);
			Utilities.saveAccuracy(accuracy(), accuracyPath);
			logger.info("Learning finished");
		} else {
			logger.warning("Training data model is empty, nothing to learn");
		}
	}

	/**
	 * Set weights loaded from collection for all neurons in all layers.
	 */
	private void setWeights() {
		if (weights != null && !weights.isEmpty()) {
			logger.info("Trying to set weights loaded from collection...");
			List<Neuron> firstLayer = new LinkedList<>(Arrays.asList(layers[0].neurons));
			List<Neuron> secondLayer = new LinkedList<>(Arrays.asList(layers[1].neurons));
			List<Neuron> thirdLayer = new LinkedList<>(Arrays.asList(layers[2].neurons));
			List<Neuron> allNeurons = new LinkedList<>();
			allNeurons.addAll(firstLayer);
			allNeurons.addAll(secondLayer);
			allNeurons.addAll(thirdLayer);

			for (int i = 0; i < weights.size(); i++) {
				String[] lineOfWeights = weights.get(i).split("\\s+");
				allNeurons.get(i).setWeights(lineOfWeights);
			}

			logger.info("Weights loaded from collection set");
		} else {
			logger.warning("Weights list is empty, nothing to load");
		}
	}

}
