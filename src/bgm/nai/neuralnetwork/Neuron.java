package bgm.nai.neuralnetwork;

import bgm.nai.logger.CustomLogger;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Single neuron representation.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */

public class Neuron {

	private static CustomLogger cl = new CustomLogger(Neuron.class.getName());
	private static Logger logger = cl.getLogger();

	private double[] weights;
	private double threshold;
	private double error;

	/**
	 * Neuron constructor.
	 *
	 * @param numberOfWeights number of neuron weights
	 */
	public Neuron(int numberOfWeights) {
		this.weights = new double[numberOfWeights];
		initialize();
	}

	/**
	 * Get access to neuron error.
	 *
	 * @return double neuron error
	 */
	public double getError() {
		return error;
	}

	/**
	 * Get access to neuron threshold.
	 *
	 * @return double neuron threshold
	 */
	public double getThreshold() {
		return threshold;
	}

	/**
	 * Get access to neuron weights.
	 *
	 * @return double[] neuron weights
	 */
	public double[] getWeights() {
		return weights;
	}

	/**
	 * Calculate output signal for given vector (constant sigmoid
	 * bipolar function).
	 *
	 * @param inputVector input vector
	 * @return double neuron output signal
	 */
	public double outputSignal(double[] inputVector) {
		logger.info("Calculating output signal...");
		double net = Utilities.multiplyVectors(weights, inputVector) + threshold;
		logger.log(Level.INFO, "Calculated NET: {0}", net);
		double signal = (2 / (1 + Math.exp(-net))) - 1;
		logger.log(Level.INFO, "Calculated output signal: {0}", signal);

		return signal;
	}

	/**
	 * Set initial neuron state.
	 */
	private void initialize() {
		logger.info("Generating random weights and threshold for neuron <-1, 1>...");
		Random random = new Random();

		for (int i = 0; i < weights.length; i++) {
			weights[i] = random.nextDouble() * 2 - 1;
		}

		threshold = random.nextDouble() * 2 - 1;
		logger.info("Generated");
	}

	/**
	 * Set neuron error.
	 *
	 * @param err calculated error for neuron
	 */
	public void setError(double err) {
		error = err;
	}

	/**
	 * Set new weights for neuron.
	 *
	 * @param lineOfWeights line of weights for neuron read from
	 *                      file
	 */
	public void setWeights(String[] lineOfWeights) {
		logger.info("Setting new weights for neuron...");
		double[] newWeights = new double[weights.length];

		for (int i = 0; i < lineOfWeights.length; i++) {
			newWeights[i] = new Double(lineOfWeights[i]);
		}

		logger.info("Weights set");

		weights = newWeights;
	}

	/**
	 * Update neuron weights and threshold.
	 *
	 * @param learningRate neural network learning rate
	 * @param inputVector  input vector for neuron
	 */
	public void updateWeights(double learningRate, double[] inputVector) {
		logger.info("Updating neuron weights and threshold...");
		weights = Utilities.addVectors(weights, Utilities.multiplyVector(inputVector, (learningRate * error)));
		threshold = threshold + (learningRate * error);
		logger.info("Weights and threshold updated");
	}

}
