package bgm.nai.neuralnetwork;

import bgm.nai.logger.CustomLogger;
import java.util.logging.Logger;

/**
 * Output layer for multilayer neural network.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class OutputLayer extends Layer {

	private static CustomLogger cl = new CustomLogger(OutputLayer.class.getName());
	private static Logger logger = cl.getLogger();

	/**
	 * OutputLayer constructor.
	 *
	 * @param numberOfNeurons number of neurons in output
	 *                        layer
	 * @param numberOfWeights number of weights for neurons
	 *                        in output layer
	 */
	public OutputLayer(int numberOfNeurons, int numberOfWeights) {
		super(numberOfNeurons, numberOfWeights);
	}

	/**
	 * Calculate error for each neuron in layer (hidden layer usage).
	 *
	 * @param nextLayer next layer in multilayer neural network
	 */
	@Override
	public void neuronsError(Neuron[] nextLayer) {
	}

	/**
	 * Calculate error for each neuron in output layer.
	 *
	 * @param answer proper answer for given training example
	 *               as vector
	 */
	@Override
	public void neuronsError(double[] answer) {
		logger.info("Calculating errors in output layer...");

		for (int i = 0; i < super.output.length; i++) {
			double derivative = 0.5 * (1 - Math.pow(super.output[i], 2));
			super.neurons[i].setError(derivative * (answer[i] - super.output[i]));
		}

		logger.info("Errors in output layer calculated");
	}

}
