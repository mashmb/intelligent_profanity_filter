package bgm.nai.neuralnetwork;

import bgm.nai.logger.CustomLogger;
import java.awt.BasicStroke;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * Utilities and tools for neural network.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class Utilities {

	private static CustomLogger cl = new CustomLogger(Utilities.class.getName());
	private static Logger logger = cl.getLogger();

	private static DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy(HH:mm:ss)");
	private static char[] alphabet = {'a', 'ą', 'b', 'c', 'ć', 'd', 'e', 'ę', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'ł', 'm', 'n', 'ń', 'o', 'ó', 'p', 'q',
		'r', 's', 'ś', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ź', 'ż'};

	/**
	 * Add two vectors with equal length.
	 *
	 * @param firstVector  first vector to create sum
	 * @param secondVector second vector to create sum
	 * @return double[] sum of two vectors
	 */
	public static double[] addVectors(double[] firstVector, double[] secondVector) {
		double[] resultVector = new double[firstVector.length];

		for (int i = 0; i < firstVector.length; i++) {
			resultVector[i] = firstVector[i] + secondVector[i];
		}

		return resultVector;
	}

	/**
	 * Encode given word to vector representation.
	 *
	 * @param word word to encode
	 * @return double[] encoded word as vector
	 */
	public static double[] encodeWord(String word) {
		double[] encoded = new double[35];
		String[] preEncoded = new String[35];
		word = word.toLowerCase().trim();
		char[] letters = word.toCharArray();
		int unknownCounter = 0;

		for (int i = 0; i < letters.length; i++) {
			int position = -1;

			for (int j = 0; j < alphabet.length; j++) {

				if (letters[i] == alphabet[j]) {
					position = j;
					break;
				}

			}

			if (position == -1) {
				unknownCounter++;
			}

			if (position != -1) {

				if (preEncoded[position] != null) {
					preEncoded[position] += (i + 1 - unknownCounter);
				} else {
					preEncoded[position] = Integer.toString(i + 1 - unknownCounter);
				}

			}

		}

		for (int i = 0; i < preEncoded.length; i++) {
			if (preEncoded[i] != null) {
				encoded[i] = new Double(preEncoded[i]);
			} else {
				encoded[i] = 0;
			}
		}

		return encoded;
	}

	/**
	 * Load training data model from file.
	 *
	 * @param path path to training data model file
	 * @return Map list of words with their classification label
	 */
	public static Map<String, Double> loadTrainingModel(String path) {
		logger.log(Level.INFO, "Trying to load training data model from file: {0}...", path);
		Map<String, Double> trainingModel = new TreeMap<>();

		try {
			FileReader fr = new FileReader(path);
			BufferedReader br = new BufferedReader(fr);
			String line;

			while ((line = br.readLine()) != null) {
				String[] trainingRow = line.split(":");

				if (trainingRow[1].toLowerCase().trim().equals("negative")) {
					trainingModel.put(trainingRow[0].toLowerCase().trim(), -1.0);
				} else if (trainingRow[1].toLowerCase().trim().equals("positive")) {
					trainingModel.put(trainingRow[0].toLowerCase().trim(), 1.0);
				}
			}

			br.close();
		} catch (IOException ex) {
			logger.severe("Cannot load training data model");
			ex.printStackTrace();
			System.exit(1);
		}

		logger.info("Training data model loaded");

		return trainingModel;
	}

	/**
	 * Load neural network weights for all layers from file.
	 *
	 * @param path path to weights file
	 * @return List list of weights for all layers in neural network
	 */
	public static List<String> loadWeights(String path) {
		logger.log(Level.INFO, "Trying to load weights from file: {0}...", path);
		List<String> weightsList = new LinkedList<>();

		try {
			FileReader fr = new FileReader(path);
			BufferedReader br = new BufferedReader(fr);
			String line;

			while ((line = br.readLine()) != null) {
				weightsList.add(line);
			}

			br.close();
		} catch (IOException ex) {
			logger.severe("Cannot load weights");
			ex.printStackTrace();
			System.exit(1);
		}

		logger.info("Weights loaded");

		return weightsList;
	}

	/**
	 * Multiply vector by constant value.
	 *
	 * @param vector   vector to multiply
	 * @param constant constant value
	 * @return double[] vector multiplied by constant value
	 */
	public static double[] multiplyVector(double[] vector, double constant) {
		double[] multiplied = new double[vector.length];

		for (int i = 0; i < vector.length; i++) {
			multiplied[i] = vector[i] * constant;
		}

		return multiplied;
	}

	/**
	 * Multiply two vectors with equal length.
	 *
	 * @param firstVector  first vector to multiply
	 * @param secondVector second vector to multiply
	 * @return double multiplication of two vectors
	 */
	public static double multiplyVectors(double[] firstVector, double[] secondVector) {
		double result = 0;

		for (int i = 0; i < firstVector.length; i++) {
			result += firstVector[i] * secondVector[i];
		}

		return result;
	}

	/**
	 * Generate chart from neural network learning process.
	 *
	 * @param chartData coordinates from neural network learning
	 *                  process (epochs and network error)
	 * @param path      path to chart saving location
	 */
	public static void generateChart(XYSeries chartData, String path) {
		if (path != null) {
			Date date = new Date();
			String fileName = "chart" + dateFormat.format(date) + ".jpeg";
			File chart = new File(path + fileName);
			logger.log(Level.INFO, "Trying to generate chart: {0}...", (path + fileName));
			XYSeriesCollection convertedChartData = new XYSeriesCollection(chartData);
			JFreeChart lineChart = ChartFactory.createXYLineChart("Neural network error in learning process",
				"Epochs", "Error", convertedChartData, PlotOrientation.VERTICAL,
				true, true, false);
			XYLineAndShapeRenderer lineRenderer = new XYLineAndShapeRenderer();
			lineRenderer.setSeriesPaint(0, Color.RED);
			lineRenderer.setSeriesStroke(0, new BasicStroke(2));
			lineRenderer.setSeriesShapesVisible(0, false);
			XYPlot plot = (XYPlot) lineChart.getPlot();
			plot.setBackgroundPaint(Color.WHITE);
			plot.setRenderer(lineRenderer);
			int imgWidth = 800;
			int imgHeight = 600;

			try {
				ChartUtilities.saveChartAsJPEG(chart, lineChart, imgWidth, imgHeight);
				logger.info("Chart generated");
			} catch (IOException ex) {
				logger.severe("Cannot generate chart");
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Save neural network accuracy print to file.
	 *
	 * @param print neural network accuracy print
	 * @param path  path to save location (folder)
	 */
	public static void saveAccuracy(String print, String path) {
		if (path != null) {
			Date date = new Date();
			String fileName = "acc" + dateFormat.format(date) + ".txt";
			logger.log(Level.INFO, "Trying to save neural network accuracy to file: {0}...", (path + fileName));

			try {
				Files.write(Paths.get(path + fileName), print.getBytes());
				logger.info("Neural network accuracy saved");
			} catch (Exception ex) {
				logger.severe("Cannot save neural network accuracy");
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Save training data model to file.
	 *
	 * @param trainingModel training data model from neural
	 *                      network
	 * @param path          path to save location (folder)
	 */
	public static void saveTrainingModel(Map<String, Double> trainingModel, String path) {
		Date date = new Date();
		String fileName = "t" + dateFormat.format(date) + ".data";
		StringBuilder sb = new StringBuilder();
		logger.log(Level.INFO, "Trying to save training data model to file: {0}...",
			new Object[]{(path + fileName)});

		if (trainingModel != null && !trainingModel.isEmpty()) {
			for (Map.Entry<String, Double> trainingRow : trainingModel.entrySet()) {
				String value = null;

				if (trainingRow.getValue() == -1) {
					value = "negative";
				} else if (trainingRow.getValue() == 1) {
					value = "positive";
				}

				sb.append(trainingRow.getKey()).append(":").append(value).append("\n");
			}

			try {
				Files.write(Paths.get(path + fileName), sb.toString().getBytes());
				logger.info("Training data model saved");
			} catch (IOException ex) {
				logger.severe("Cannot save training data model");
				ex.printStackTrace();
				System.exit(1);
			}
		} else {
			logger.warning("Training data model empty, nothing to save");
		}
	}

	/**
	 * Save neural network weights from all layers to file for future
	 * usage.
	 *
	 * @param weights weights from all layers of neural network
	 * @param path    path to save location (folder)
	 */
	public static void saveWeights(List<String> weights, String path) {
		Date date = new Date();
		String fileName = "w" + dateFormat.format(date) + ".weights";
		logger.log(Level.INFO, "Trying to save neural network weights from all layers to file: {0}...",
			new Object[]{(path + fileName)});
		StringBuilder sb = new StringBuilder();

		for (String row : weights) {
			sb.append(row);
		}

		try {
			Files.write(Paths.get(path + fileName), sb.toString().getBytes());
			logger.info("Neural network weights from all layers saved");
		} catch (IOException ex) {
			logger.severe("Cannot save neural network weights from all layers");
			ex.printStackTrace();
			System.exit(1);
		}
	}

}
