package bgm.nai.server;

import bgm.nai.logger.CustomLogger;
import java.rmi.RemoteException;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;

/**
 * Main entry point for group chat server.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class ChatServer {

	private static CustomLogger cl = new CustomLogger(ChatServer.class.getName());
	private static Logger logger = cl.getLogger();

	/**
	 * Initialize group chat server program.
	 *
	 * @param args program arguments
	 */
	public static void main(String[] args) {

		SwingUtilities.invokeLater(() -> {

			try {
				Model model = new Model();
				View view = new View();
				ChatServerImpl networkDriver = new ChatServerImpl(model, view);
				Controller controller = new Controller(model, view, networkDriver);
				controller.init();
			} catch (RemoteException ex) {
				logger.severe("Cannot launch group chat server");
				ex.printStackTrace();
			}

		});

	}

}
