package bgm.nai.server;

import bgm.nai.logger.CustomLogger;
import bgm.nai.neuralnetwork.NeuralNetwork;
import bgm.nai.shared.interfaces.ChatClientInt;
import bgm.nai.shared.interfaces.ChatServerInt;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * Implementation of group chat server, network layer.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class ChatServerImpl extends UnicastRemoteObject
	implements ChatServerInt {

	private static CustomLogger cl = new CustomLogger(ChatServerImpl.class.getName());
	private static Logger logger = cl.getLogger();

	private Model model;
	private View view;

	/**
	 * ChatServerImpl constructor.
	 *
	 * @param model access to data layer
	 * @param view  access to view layer
	 * @throws RemoteException exception thrown while problem
	 *                         with network communication occur
	 */
	protected ChatServerImpl(Model model, View view) throws RemoteException {
		this.model = model;
		this.view = view;
	}

	/**
	 * Register new client in group chat server database for future
	 * data sharing.
	 *
	 * @param client    list of remote methods available on
	 *                  connected client site
	 * @param nickname  nickname for which client will be visible
	 *                  during conversation
	 * @param ipAddress IP address of connected client
	 * @return boolean logical value if client connected successfully
	 * @throws RemoteException exception thrown while problem
	 *                         with network communication occur
	 */
	@Override
	public boolean connect(ChatClientInt client, String nickname, String ipAddress) throws RemoteException {
		logger.info("Handling new register request...");
		String fullClientName = nickname + "@" + ipAddress;

		if (model.getClients().containsKey(fullClientName)) {
			logger.warning("Registration failed, client already exists");

			return false;
		} else {
			model.getClients().put(fullClientName, client);
			view.getMessagesBox().getTextarea().append(fullClientName + " connected\n");
			logger.log(Level.INFO, "Client [{0}] successfully registered", fullClientName);
			broadcastMessage(fullClientName, fullClientName + " connected to the chat room");

			return true;
		}
	}

	/**
	 * Unregister already connected client from the group chat server.
	 *
	 * @param nickname  nickname of client that sends request
	 * @param ipAddress IP address of client that sends request
	 * @return boolean logical value if client disconnected properly
	 * @throws RemoteException exception thrown while problem
	 *                         with network communication occur
	 */
	@Override
	public boolean disconnect(String nickname, String ipAddress) throws RemoteException {
		logger.info("Handling disconnect request...");
		String fullClientName = nickname + "@" + ipAddress;

		if (model.getClients().containsKey(fullClientName)) {
			model.getClients().remove(fullClientName);
			view.getMessagesBox().getTextarea().append(fullClientName + " disconnected\n");
			broadcastMessage(fullClientName, fullClientName + " disconnected from chat room");
			logger.log(Level.INFO, "Client [{0}] disconnected properly", fullClientName);

			return true;
		} else {
			logger.warning("Cannot disconnect client that is not connected to the server");

			return false;
		}
	}

	/**
	 * Broadcast new message to all clients.
	 *
	 * @param sender  sender nickname
	 * @param message new message
	 */
	private void broadcastMessage(String sender, String message) {
		String fullMessage = "[" + sender + "]: " + message;
		logger.info("Broadcasting message to clients...");

		for (Map.Entry<String, ChatClientInt> client : model.getClients().entrySet()) {

			try {
				client.getValue().updateMessageBox(fullMessage);
			} catch (RemoteException ex) {
				logger.severe("Problem with connection to client occurred");
				ex.printStackTrace();
			}

		}
		logger.info("Broadcast ended");
	}

	/**
	 * Find proper IP address for network communication (proper
	 * interface).
	 */
	public void detectInterface() {
		logger.info("Trying to get IP address for network communication...");

		try {
			Socket validator = new Socket();
			validator.connect(new InetSocketAddress("google.com", 80));
			String ip = validator.getLocalAddress().getHostAddress();
			validator.close();
			logger.log(Level.INFO, "Proper network interface detected: {0}", ip);
			model.setLocalhost(ip);
		} catch (IOException ex) {
			logger.severe("There are no network interfaces that meet the requirements");
			ex.printStackTrace();
		}
	}

	/**
	 * Use neural network to verify message (profanity filtering) and
	 * send message to rest of the clients.
	 *
	 * @param message   message from client
	 * @param nickname  nickname of client that sends message
	 * @param ipAddress IP address of client that sends message
	 * @throws RemoteException exception thrown while problem
	 *                         with network communication occur
	 */
	@Override
	public void verifyMessage(String message, String nickname, String ipAddress) throws RemoteException {
		String fullClientName = nickname + "@" + ipAddress;
		view.getMessagesBox().getTextarea().append("[" + fullClientName + "]: " + message + "\n");
		List<String> tokens = new LinkedList<>();
		Scanner scanner = new Scanner(message);
		StringBuilder filteredMessage = new StringBuilder();

		if (model.getNeedChangeFFN() && !model.getAdditionalFFN().isLearning()) {
			view.getNeuralNetworkBox().getTextarea().append("Additional neural network finished learning\n");
			model.setMainFFN(model.getAdditionalFFN());
			model.setAdditionalFFN(new NeuralNetwork(35, 1,
				model.getMainFFN().getAllWeights(), model.getMainFFN().getTrainingModel()));
			view.getNeuralNetworkBox().getTextarea().append("Main neural network replaced with additional\n");
			model.setNeedChangeFFN(false);
		}

		while (scanner.hasNext()) {
			tokens.add(scanner.next());
		}

		for (String token : tokens) {
			if (token.length() > 2) {
				String answer = model.getMainFFN().predict(token);

				switch (answer) {
					case "positive":
						filteredMessage.append(token).append(" ");
						view.getNeuralNetworkBox().getTextarea().append(token + ": " + answer + "\n");
						break;

					case "negative":
						StringBuilder filteredWord = new StringBuilder();

						for (int i = 0; i < token.length(); i++) {
							if (i < token.length() - 2) {
								filteredWord.append("*");
							} else {
								filteredWord.append(token.charAt(i));
							}
						}

						filteredMessage.append(filteredWord).append(" ");
						view.getNeuralNetworkBox().getTextarea().append(token + ": " + answer + "\n");
						break;

					case "unknown":
						filteredMessage.append(token).append(" ");
						Object[] options = {"Positive", "Negative"};
						int userAnswer = JOptionPane.showOptionDialog(view.getWindow(),
							"Unknown word: " + token + ".\n User action needed.",
							"Unknown word",
							JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE,
							null, options, null);

						if (userAnswer == 0) {
							model.getWordsQueue().put(token, 1.0);
						} else if (userAnswer == 1) {
							model.getWordsQueue().put(token, -1.0);
						}

						model.setNeedChangeFFN(true);
						view.getNeuralNetworkBox().getTextarea().append(token + ": " + answer + "\n");
						break;
				}
			} else {
				filteredMessage.append(token).append(" ");
			}
		}

		if (!model.getWordsQueue().isEmpty() && !model.getAdditionalFFN().isLearning()) {
			model.setAdditionalFFN(new NeuralNetwork(35, 1,
				model.getMainFFN().getAllWeights(), model.getMainFFN().getTrainingModel()));
			model.getAdditionalFFN().appendToTrainingModel(model.getWordsQueue());
			model.resetWordsQueue();
			new Thread(() -> model.getAdditionalFFN().learn(model.getChartsPath(), model.getAccuracyPath())).start();
			view.getNeuralNetworkBox().getTextarea().append("Additional neural network learning...\n");
		}

		broadcastMessage(fullClientName, filteredMessage.toString());
	}

}
