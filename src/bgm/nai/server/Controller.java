package bgm.nai.server;

import bgm.nai.logger.CustomLogger;
import bgm.nai.neuralnetwork.NeuralNetwork;
import bgm.nai.neuralnetwork.Utilities;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Control layer for group chat server.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class Controller {

	private static CustomLogger cl = new CustomLogger(Controller.class.getName());
	private static Logger logger = cl.getLogger();

	private Model model;
	private View view;
	private ChatServerImpl networkDriver;

	private Registry registry;

	/**
	 * Controller constructor.
	 *
	 * @param model         access to data layer
	 * @param view          access to view layer
	 * @param networkDriver access to network layer
	 */
	public Controller(Model model, View view, ChatServerImpl networkDriver) {
		this.model = model;
		this.view = view;
		this.networkDriver = networkDriver;
	}

	/**
	 * Initialize group chat server program and controls listeners.
	 */
	public void init() {
		ActionListener buttonsListener = ActionEvent -> {

			switch (ActionEvent.getActionCommand()) {
				case "start":
					start();
					break;

				case "stop":
					stop();
					break;
			}

		};
		view.getStartButton().addActionListener(buttonsListener);
		view.getStopButton().addActionListener(buttonsListener);

		networkDriver.detectInterface();
		view.getIpLabel().setText(model.getLocalhost());
		chooseFiles();
	}

	/**
	 * Choose training data model file and weights file, initialize
	 * main feed forward neural network.
	 */
	private void chooseFiles() {
		FileNameExtensionFilter trainingFilter = new FileNameExtensionFilter("Training Models", "data");
		JFileChooser trainingFileChooser = new JFileChooser();
		trainingFileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
		trainingFileChooser.setFileFilter(trainingFilter);
		FileNameExtensionFilter weightsFilter = new FileNameExtensionFilter("Weight Files", "weights");
		JFileChooser weightsFileChooser = new JFileChooser();
		weightsFileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
		weightsFileChooser.setFileFilter(weightsFilter);
		int trainingFileValue = trainingFileChooser.showOpenDialog(view.getWindow());
		int weightsFileValue = weightsFileChooser.showOpenDialog(view.getWindow());
		Map<String, Double> trainingModel = null;
		List<String> weights = null;

		if (trainingFileValue == JFileChooser.APPROVE_OPTION) {
			trainingModel = Utilities.loadTrainingModel(trainingFileChooser.getSelectedFile().toString());
		}

		if (weightsFileValue == JFileChooser.APPROVE_OPTION) {
			weights = Utilities.loadWeights(weightsFileChooser.getSelectedFile().toString());
		}

		model.setMainFFN(new NeuralNetwork(35, 1, weights, trainingModel));
	}

	/**
	 * Start group chat server (RMI protocol).
	 */
	private void start() {
		logger.info("Trying to start group chat server...");

		Object[] options = {"Yes", "No"};
		int userAnswer = JOptionPane.showOptionDialog(view.getWindow(),
			"Is neural network learning needed?",
			"Neural network",
			JOptionPane.YES_NO_OPTION,
			JOptionPane.QUESTION_MESSAGE,
			null, options, null);

		if (userAnswer == 0) {
			view.getNeuralNetworkBox().getTextarea().append("Main neural network learning...\n");
			model.getMainFFN().learn(model.getChartsPath(), model.getAccuracyPath());
			view.getNeuralNetworkBox().getTextarea().append("Main neural network finished learning\n");
		}

		model.setAdditionalFFN(new NeuralNetwork(35, 1,
			model.getMainFFN().getAllWeights(), model.getMainFFN().getTrainingModel()));

		try {

			if (registry == null) {
				registry = LocateRegistry.createRegistry(model.getPort());
			}

			registry.rebind("rmi://" + model.getLocalhost() + ":" + model.getPort() + "/groupChat", networkDriver);
			view.getMessagesBox().getTextarea().append("Server started: " + model.getLocalhost() + ":" +
				model.getPort() + "\n");
			view.getStartButton().setEnabled(false);
			view.getStopButton().setEnabled(true);
			logger.log(Level.INFO, "Group chat server started: {0}:{1}",
				new Object[]{model.getLocalhost(), model.getPort()});

		} catch (RemoteException ex) {
			view.getStartButton().setEnabled(true);
			view.getStopButton().setEnabled(false);
			logger.severe("Cannot start group chat server");
			ex.printStackTrace();
		}
	}

	/**
	 * Stop group chat server (RMI protocol).
	 */
	private void stop() {
		logger.info("Trying to stop group chat server...");

		if (!model.getMainFFN().isLearning()) {
			view.getNeuralNetworkBox().getTextarea().append("Saving weights and training model...\n");
			Utilities.saveWeights(model.getMainFFN().getAllWeights(), model.getWeightsPath());
			Utilities.saveTrainingModel(model.getMainFFN().getTrainingModel(), model.getTrainingModelsPath());
			view.getNeuralNetworkBox().getTextarea().append("Saved\n");
		}

		try {

			registry.unbind("rmi://" + model.getLocalhost() + ":" + model.getPort() + "/groupChat");
			view.getMessagesBox().getTextarea().append("Server stopped\n");
			view.getStopButton().setEnabled(false);
			view.getStartButton().setEnabled(true);
			logger.info("Group chat server stopped");
			view.getWindow().dispatchEvent(new WindowEvent(view.getWindow(), WindowEvent.WINDOW_CLOSING));

		} catch (Exception ex) {
			view.getStopButton().setEnabled(true);
			view.getStartButton().setEnabled(false);
			logger.severe("Cannot stop group chat server");
			ex.printStackTrace();
		}
	}

}
