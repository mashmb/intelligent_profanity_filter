package bgm.nai.server;

import bgm.nai.neuralnetwork.NeuralNetwork;
import bgm.nai.shared.interfaces.ChatClientInt;
import java.io.File;
import java.util.Map;
import java.util.TreeMap;

/**
 * Data layer for group chat server.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class Model {

	private NeuralNetwork mainFFN;
	private NeuralNetwork additionalFFN;
	private String localhost = null;
	private boolean needChangeFFN = false;
	private Map<String, ChatClientInt> clients = new TreeMap<>();
	private Map<String, Double> wordsQueue = new TreeMap<>();
	private String trainingModelsPath = System.getProperty("user.dir") + File.separator + "assets" +
		File.separator + "training_data" + File.separator;
	private String weightsPath = System.getProperty("user.dir") + File.separator + "assets" +
		File.separator + "weights" + File.separator;
	private String chartsPath = System.getProperty("user.dir") + File.separator + "assets" +
		File.separator + "charts" + File.separator;
	private String accuracyPath = System.getProperty("user.dir") + File.separator + "assets" +
		File.separator + "accuracy" + File.separator;

	/**
	 * Get access to saved neural network accuracy prints location
	 * (folder).
	 *
	 * @return String saved neural network accuracy prints path
	 */
	public String getAccuracyPath() {
		return accuracyPath;
	}

	/**
	 * Get access to additional neural network.
	 *
	 * @return NeuralNetwork additional neural network
	 */
	public NeuralNetwork getAdditionalFFN() {
		return additionalFFN;
	}

	/**
	 * Get access to localization with generated charts.
	 *
	 * @return String path to generated charts folder
	 */
	public String getChartsPath() {
		return chartsPath;
	}

	/**
	 * Get access to registered clients list.
	 *
	 * @return Map list of registered clients
	 */
	public Map<String, ChatClientInt> getClients() {
		return clients;
	}

	/**
	 * Get access to group chat server IP address.
	 *
	 * @return String group chat server IP address
	 */
	public String getLocalhost() {
		return localhost;
	}

	/**
	 * Get access to main neural network.
	 *
	 * @return NeuralNetwork main neural network
	 */
	public NeuralNetwork getMainFFN() {
		return mainFFN;
	}

	/**
	 * Get access to status of need to change neural network.
	 *
	 * @return boolean need change status
	 */
	public boolean getNeedChangeFFN() {
		return needChangeFFN;
	}

	/**
	 * Get access to group chat server port.
	 *
	 * @return int group chat server port
	 */
	public int getPort() {
		return 50000;
	}

	/**
	 * Get access to saved training models location (folder).
	 *
	 * @return String saved training models path
	 */
	public String getTrainingModelsPath() {
		return trainingModelsPath;
	}

	/**
	 * Get access to saved weights location (folder).
	 *
	 * @return String saved weights path
	 */
	public String getWeightsPath() {
		return weightsPath;
	}

	/**
	 * Get access to words queue.
	 *
	 * @return Map words queue
	 */
	public Map<String, Double> getWordsQueue() {
		return wordsQueue;
	}

	/**
	 * Reset words queue (make it empty).
	 */
	public void resetWordsQueue() {
		wordsQueue = new TreeMap<>();
	}

	/**
	 * Set additional neural network.
	 *
	 * @param nn additional neural network
	 */
	public void setAdditionalFFN(NeuralNetwork nn) {
		additionalFFN = nn;
	}

	/**
	 * Set IP address of localhost.
	 *
	 * @param ip IP address
	 */
	public void setLocalhost(String ip) {
		localhost = ip;
	}

	/**
	 * Set main neural network.
	 *
	 * @param nn main neural network
	 */
	public void setMainFFN(NeuralNetwork nn) {
		mainFFN = nn;
	}

	/**
	 * Change status of need change neural network.
	 *
	 * @param status status as logical value
	 */
	public void setNeedChangeFFN(boolean status) {
		needChangeFFN = status;
	}

}
