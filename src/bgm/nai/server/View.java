package bgm.nai.server;

import bgm.nai.logger.CustomLogger;
import bgm.nai.server.components.AutoScrollTextBox;
import bgm.nai.server.components.Window;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

/**
 * View layer for the group chat server.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class View {

	private static CustomLogger cl = new CustomLogger(View.class.getName());
	private static Logger logger = cl.getLogger();

	private Window window;

	private AutoScrollTextBox messagesBox;
	private AutoScrollTextBox neuralNetworkBox;

	private JLabel ipLabel;

	private JButton startButton;
	private JButton stopButton;

	/**
	 * View constructor and components settings.
	 */
	public View() {
		setUI();

		this.window = new Window();
		this.messagesBox = new AutoScrollTextBox();
		this.neuralNetworkBox = new AutoScrollTextBox();
		JLabel ipTitleLabel = new JLabel("IP address:");
		this.ipLabel = new JLabel();
		JLabel messagesLabel = new JLabel("Messages");
		JLabel neuralNetworkLabel = new JLabel("Neural network");
		this.startButton = new JButton("START");
		this.stopButton = new JButton("STOP");

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;

		// ipTitleLabel settings
		ipTitleLabel.setPreferredSize(new Dimension(300, 30));
		ipTitleLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		ipTitleLabel.setFont(new Font("Arial", Font.PLAIN, 16));
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 30;
		gbc.gridheight = 3;
		this.window.add(ipTitleLabel, gbc);

		// ipLabel settings
		this.ipLabel.setPreferredSize(new Dimension(300, 30));
		this.ipLabel.setHorizontalAlignment(SwingConstants.LEFT);
		this.ipLabel.setFont(new Font("Arial", Font.PLAIN, 16));
		gbc.gridx = 30;
		gbc.gridy = 0;
		gbc.gridwidth = 30;
		gbc.gridheight = 3;
		this.window.add(this.ipLabel, gbc);

		// messagesLabel settings
		messagesLabel.setPreferredSize(new Dimension(300, 30));
		messagesLabel.setHorizontalAlignment(SwingConstants.CENTER);
		messagesLabel.setVerticalAlignment(SwingConstants.BOTTOM);
		messagesLabel.setFont(new Font("Arial", Font.PLAIN, 16));
		gbc.gridx = 0;
		gbc.gridy = 6;
		gbc.gridwidth = 30;
		gbc.gridheight = 3;
		this.window.add(messagesLabel, gbc);

		// neuralNetworkLabel settings
		neuralNetworkLabel.setPreferredSize(new Dimension(300, 30));
		neuralNetworkLabel.setHorizontalAlignment(SwingConstants.CENTER);
		neuralNetworkLabel.setVerticalAlignment(SwingConstants.BOTTOM);
		neuralNetworkLabel.setFont(new Font("Arial", Font.PLAIN, 16));
		gbc.gridx = 30;
		gbc.gridy = 6;
		gbc.gridwidth = 30;
		gbc.gridheight = 3;
		this.window.add(neuralNetworkLabel, gbc);

		// messagesBox settings
		gbc.gridx = 0;
		gbc.gridy = 9;
		gbc.gridwidth = 30;
		gbc.gridheight = 31;
		this.window.add(this.messagesBox, gbc);

		// neuralNetworkBox settings
		gbc.gridx = 30;
		gbc.gridy = 9;
		gbc.gridwidth = 30;
		gbc.gridheight = 31;
		this.window.add(this.neuralNetworkBox, gbc);

		// startButton settings
		this.startButton.setPreferredSize(new Dimension(300, 30));
		this.startButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
		this.startButton.setActionCommand("start");
		this.startButton.setToolTipText("Start neural network learning and chat server");
		this.startButton.setFont(new Font("Arial", Font.PLAIN, 16));
		gbc.gridx = 0;
		gbc.gridy = 40;
		gbc.gridwidth = 30;
		gbc.gridheight = 3;
		this.window.add(this.startButton, gbc);

		// stopButton settings
		this.stopButton.setPreferredSize(new Dimension(300, 30));
		this.stopButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
		this.stopButton.setActionCommand("stop");
		this.stopButton.setEnabled(false);
		this.stopButton.setToolTipText("Stop neural network learning and chat server");
		this.stopButton.setFont(new Font("Arial", Font.PLAIN, 16));
		gbc.gridx = 30;
		gbc.gridy = 40;
		gbc.gridwidth = 30;
		gbc.gridheight = 3;
		this.window.add(this.stopButton, gbc);
	}

	/**
	 * Get access to text box with neural network learning information.
	 *
	 * @return AutoScrollTextBox text box with neural network
	 * learning information
	 */
	public AutoScrollTextBox getNeuralNetworkBox() {
		return neuralNetworkBox;
	}

	/**
	 * Get access to label with server IP address.
	 *
	 * @return JLabel label with server IP address
	 */
	public JLabel getIpLabel() {
		return ipLabel;
	}

	/**
	 * Get access to text box with messages that server receives.
	 *
	 * @return AutoScrollTextBox text box with messages that server
	 * receives
	 */
	public AutoScrollTextBox getMessagesBox() {
		return messagesBox;
	}

	/**
	 * Get access to button that starts chat server.
	 *
	 * @return JButton button that starts chat server
	 */
	public JButton getStartButton() {
		return startButton;
	}

	/**
	 * Get access to button that stops chat server.
	 *
	 * @return JButton button that stops chat server
	 */
	public JButton getStopButton() {
		return stopButton;
	}

	/**
	 * Get access to group chat server window.
	 *
	 * @return Window group chat server window
	 */
	public Window getWindow() {
		return window;
	}

	/**
	 * Set system theme for Java Swing components.
	 */
	private void setUI() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception ex) {
			logger.severe("Cannot set system theme for Java Swing components");
			ex.printStackTrace();
		}
	}

}
