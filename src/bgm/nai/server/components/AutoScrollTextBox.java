package bgm.nai.server.components;

import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

/**
 * Scroll pane with text box with auto scroll feature.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class AutoScrollTextBox extends JScrollPane {

	private JTextArea textarea = new JTextArea();

	/**
	 * AutoScrollTextBox constructor and components settings.
	 */
	public AutoScrollTextBox() {
		// textarea settings
		this.textarea.setEditable(false);
		this.textarea.setLineWrap(true);
		this.textarea.setWrapStyleWord(true);
		this.textarea.setFont(new Font("Arial", Font.PLAIN, 12));
		DefaultCaret caret = (DefaultCaret) this.textarea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		// AutoScrollTextBox settings
		this.setPreferredSize(new Dimension(300, 340));
		this.setViewportView(this.textarea);
	}

	/**
	 * Get access to text area inside scroll pane.
	 *
	 * @return JTextArea text area inside scroll pane
	 */
	public JTextArea getTextarea() {
		return textarea;
	}

}
