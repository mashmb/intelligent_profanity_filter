package bgm.nai.server.components;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

/**
 * Main window for chat server.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class Window extends JFrame {

	/**
	 * Window constructor and settings.
	 */
	public Window() {
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(640, 480));
		this.setTitle("Chat Server");
		this.setLayout(new GridBagLayout());
		this.setResizable(false);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

}
