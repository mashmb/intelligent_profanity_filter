package bgm.nai.shared.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Definition of methods that group chat client provides for remote
 * communication.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public interface ChatClientInt extends Remote {

	/**
	 * Update message box with new message.
	 *
	 * @param message new message
	 * @throws RemoteException exception thrown while problem
	 *                         with network communication occur
	 */
	void updateMessageBox(String message) throws RemoteException;

}
