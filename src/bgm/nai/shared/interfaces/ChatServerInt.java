package bgm.nai.shared.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Definition of methods that group chat server provides for remote
 * communication.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public interface ChatServerInt extends Remote {

	/**
	 * Register new client in group chat server database for future
	 * data sharing.
	 *
	 * @param client    list of remote methods available on
	 *                  connected client site
	 * @param nickname  nickname for which client will be visible
	 *                  during conversation
	 * @param ipAddress IP address of connected client
	 * @return boolean logical value if client connected successfully
	 * @throws RemoteException exception thrown while problem
	 *                         with network communication occur
	 */
	boolean connect(ChatClientInt client, String nickname, String ipAddress) throws RemoteException;

	/**
	 * Unregister already connected client from the group chat server.
	 *
	 * @param nickname  nickname of client that sends request
	 * @param ipAddress IP address of client that sends request
	 * @return boolean logical value if client disconnected properly
	 * @throws RemoteException exception thrown while problem
	 *                         with network communication occur
	 */
	boolean disconnect(String nickname, String ipAddress) throws RemoteException;

	/**
	 * Use neural network to verify message (profanity filtering) and
	 * send message to rest of the clients.
	 *
	 * @param message   message from client
	 * @param nickname  nickname of client that sends message
	 * @param ipAddress IP address of client that sends message
	 * @throws RemoteException exception thrown while problem
	 *                         with network communication occur
	 */
	void verifyMessage(String message, String nickname, String ipAddress) throws RemoteException;

}
