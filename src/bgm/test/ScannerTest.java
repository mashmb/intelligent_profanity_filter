package bgm.test;

import java.util.Scanner;
import org.junit.Test;

/**
 * Tests for Scanner class build in Java.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class ScannerTest {

	/**
	 * Test Scanner class behaviour.
	 */
	@Test
	public void testWordsTokenize() {
		String first = "Cześć co tam u Ciebie słychać?";
		String second = "12.08.2016 123mars321.";
		Scanner firstScanner = new Scanner(first);
		Scanner secondScanner = new Scanner(second);

		while (firstScanner.hasNext()) {
			System.out.println(firstScanner.next());
		}

		while (secondScanner.hasNext()) {
			System.out.println(secondScanner.next());
		}

	}

}
