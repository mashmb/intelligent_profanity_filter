package bgm.test.client;

import bgm.nai.client.Model;
import org.junit.Test;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests for group chat client data layer.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class ModelTest {

	/**
	 * Testing method that validates IP addresses.
	 */
	@Test
	public void testIpValidation() {
		Model model = new Model();

		assertTrue(model.validateIp("127.0.0.1"));
		assertTrue(model.validateIp("0.0.0.0"));
		assertFalse(model.validateIp("256.21.58.1"));
		assertFalse(model.validateIp("0.0..0.0"));
		assertFalse(model.validateIp("test"));
	}

}
