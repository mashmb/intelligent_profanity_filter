package bgm.test.client;

import bgm.nai.client.View;
import javax.swing.SwingUtilities;
import org.hamcrest.CoreMatchers;
import org.junit.Test;


import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Tests for chat client view layer.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class ViewTest {

	/**
	 * Test if chat client graphical user interface shows and has
	 * correct layout.
	 *
	 * @throws InterruptedException exception can occur because
	 *                              of Thread.sleep usage
	 */
	@Test
	public void testLayout() throws InterruptedException {
		final View[] views = {null};

		SwingUtilities.invokeLater(() -> {
			views[0] = new View();
		});

		Thread.sleep(10000);
		assertThat(views[0], CoreMatchers.instanceOf(View.class));
	}

}
