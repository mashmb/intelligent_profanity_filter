package bgm.test.neuralnetwork;

import bgm.nai.neuralnetwork.NeuralNetwork;
import bgm.nai.neuralnetwork.Utilities;
import java.io.File;
import java.util.List;
import java.util.Map;
import org.junit.Test;


import static org.junit.Assert.assertEquals;

/**
 * Tests for neural network.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class NeuralNetworkTest {

	/**
	 * Test if layers of neural network have proper number of neurons.
	 */
	@Test
	public void testCreateLayers() {
		int inputLayerLength = 18;
		int hiddenLayerLength = 4;
		int outputLayerLength = 1;
		NeuralNetwork nn = new NeuralNetwork(35, 1, null, null);

		assertEquals(nn.getLayers()[0].neurons.length, inputLayerLength);
		assertEquals(nn.getLayers()[1].neurons.length, hiddenLayerLength);
		assertEquals(nn.getLayers()[2].neurons.length, outputLayerLength);
	}

	/**
	 * Test if neural network learning chart is generating properly.
	 */
	@Test
	public void testGenerateChart() {
		String modelPath = System.getProperty("user.dir") + File.separator + "assets" + File.separator + "tests" +
			File.separator + "training_data" + File.separator + "test_model.data";
		String chartPath = System.getProperty("user.dir") + File.separator + "assets" + File.separator + "tests" +
			File.separator + "charts" + File.separator;
		Map<String, Double> trainingModel = Utilities.loadTrainingModel(modelPath);
		NeuralNetwork nn = new NeuralNetwork(35, 1, null, trainingModel);
		nn.learn(chartPath, null);
	}

	/**
	 * Test if neural network predicts class of example words after
	 * learning correctly.
	 */
	@Test
	public void testLearn() {
		String path = System.getProperty("user.dir") + File.separator + "assets" + File.separator + "tests" + File.separator +
			"training_data" + File.separator + "test_model.data";
		Map<String, Double> trainingModel = Utilities.loadTrainingModel(path);
		NeuralNetwork nn = new NeuralNetwork(35, 1, null, trainingModel);
		nn.learn(null, null);

		assertEquals(nn.predict("mars"), "positive");
		assertEquals(nn.predict("sram"), "negative");
		assertEquals(nn.predict("marsjanin"), "unknown");
	}

	/**
	 * Test if weights for all neurons in neural network from file are
	 * set correctly.
	 */
	@Test
	public void testLoadWeights() {
		String path = System.getProperty("user.dir") + File.separator + "assets" + File.separator + "tests" + File.separator +
			"weights" + File.separator + "test_weights.weights";
		List<String> weights = Utilities.loadWeights(path);
		NeuralNetwork nn = new NeuralNetwork(35, 1, weights, null);
		double firstWeight = nn.getLayers()[0].neurons[0].getWeights()[0];
		double lastWeight = nn.getLayers()[2].neurons[nn.getLayers()[2].neurons.length - 1].getWeights()[0];
		double expectedFirstWeight = new Double(weights.get(0).split("\\s+")[0]);
		double expectedLastWeight = new Double(weights.get(weights.size() - 1).split("\\s+")[0]);

		assertEquals(firstWeight, expectedFirstWeight, 0.0);
		assertEquals(lastWeight, expectedLastWeight, 0.0);
	}

	/**
	 * Test if neural network accuracy print is saving properly.
	 */
	@Test
	public void testSaveAccuracy() {
		String modelPath = System.getProperty("user.dir") + File.separator + "assets" + File.separator + "tests" + File.separator +
			"training_data" + File.separator + "test_model.data";
		String accuracyPath = System.getProperty("user.dir") + File.separator + "assets" + File.separator + "tests" +
			File.separator + "accuracy" + File.separator;
		Map<String, Double> trainingModel = Utilities.loadTrainingModel(modelPath);
		NeuralNetwork nn = new NeuralNetwork(35, 1, null, trainingModel);
		nn.learn(null, accuracyPath);
	}

	/**
	 * Test if training data model is saved to file.
	 */
	@Test
	public void testSaveTrainingModel() {
		String testModelPath = System.getProperty("user.dir") + File.separator + "assets" + File.separator + "tests" +
			File.separator + "training_data" + File.separator + "test_model.data";
		String pathToSave = System.getProperty("user.dir") + File.separator + "assets" + File.separator + "tests" +
			File.separator + "training_data" + File.separator;
		Map<String, Double> trainingModel = Utilities.loadTrainingModel(testModelPath);
		NeuralNetwork nn = new NeuralNetwork(35, 1, null, trainingModel);
		Utilities.saveTrainingModel(nn.getTrainingModel(), pathToSave);
	}

	/**
	 * Test if neural network weights from all layers are saved to the
	 * file.
	 */
	@Test
	public void testSaveWeights() {
		String path = System.getProperty("user.dir") + File.separator + "assets" + File.separator + "tests" + File.separator +
			"weights" + File.separator;
		NeuralNetwork nn = new NeuralNetwork(35, 1, null, null);
		Utilities.saveWeights(nn.getAllWeights(), path);
	}

}
