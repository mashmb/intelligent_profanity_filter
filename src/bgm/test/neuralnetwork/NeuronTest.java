package bgm.test.neuralnetwork;

import bgm.nai.neuralnetwork.Neuron;
import org.junit.Test;


import static org.junit.Assert.assertTrue;

/**
 * Tests for single neuron.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class NeuronTest {

	/**
	 * Test if neuron weights and threshold are in range <-1, 1>.
	 */
	@Test
	public void testInitialize() {
		Neuron n = new Neuron(10);

		for (int i = 0; i < n.getWeights().length; i++) {
			assertTrue(n.getWeights()[i] >= -1 && n.getWeights()[i] <= 1);
		}

		assertTrue(n.getThreshold() >= -1 && n.getThreshold() <= 1);
	}

}
