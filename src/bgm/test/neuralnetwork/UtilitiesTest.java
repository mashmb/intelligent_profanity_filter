package bgm.test.neuralnetwork;

import bgm.nai.neuralnetwork.Utilities;
import java.util.Arrays;
import org.junit.Test;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests for neural network utilities and tools.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class UtilitiesTest {

	/**
	 * Test of two vectors addition.
	 */
	@Test
	public void testAddVectors() {
		double[] firstVector = {1, 2, 3, 4, 5};
		double[] secondVector = {5, 4, 3, 2, 1};
		double[] expectedResult = {6, 6, 6, 6, 6};
		double[] result = Utilities.addVectors(firstVector, secondVector);

		assertTrue(Arrays.equals(expectedResult, result));
	}

	/**
	 * Test if words encoding works properly.
	 */
	@Test
	public void testEncodeWord() {
		double[] firstWord = Utilities.encodeWord("ala");
		double[] secondWord = Utilities.encodeWord("Ala");
		double[] thirdWord = Utilities.encodeWord("kot");
		double[] fourthWord = Utilities.encodeWord("09.06.2018");
		double[] trapWord = Utilities.encodeWord("123ala321");
		double[] firstExpected = {13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		double[] secondExpected = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0};
		double[] fourthExpected = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

		assertArrayEquals(firstWord, firstExpected, 0.0);
		assertArrayEquals(secondWord, firstExpected, 0.0);
		assertArrayEquals(thirdWord, secondExpected, 0.0);
		assertArrayEquals(fourthWord, fourthExpected, 0.0);
		assertArrayEquals(trapWord, firstExpected, 0.0);
	}

	/**
	 * Test multiplying vector by constant value.
	 */
	@Test
	public void testMultiplyVector() {
		double[] vector = {1, 2, 3};
		double constant = 2;
		double[] expected = {2, 4, 6};
		double[] result = Utilities.multiplyVector(vector, constant);

		assertArrayEquals(expected, result, 0.0);
	}

	/**
	 * Test of two vectors multiplication.
	 */
	@Test
	public void testMultiplyVectors() {
		double[] firstVector = {1, 2, 3};
		double[] secondVector = {3, 2, 1};
		double expectedResult = 10;
		double result = Utilities.multiplyVectors(firstVector, secondVector);

		assertEquals(expectedResult, result, 0.0);
	}

}
