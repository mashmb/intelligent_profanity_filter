package bgm.test.server;

import bgm.nai.server.View;
import javax.swing.SwingUtilities;
import org.hamcrest.CoreMatchers;
import org.junit.Test;


import static org.junit.Assert.assertThat;

/**
 * Tests for chat server graphical user interface.
 *
 * @author Kacper Gąsior, Maciej Bedra, Sebastian Marut
 */
public class ViewTest {

	/**
	 * Test if chat server graphical user interface shows and layout is
	 * correct.
	 *
	 * @throws InterruptedException exception that can occur because
	 *                              of usage of Thread.sleep()
	 */
	@Test
	public void testLayout() throws InterruptedException {
		final View[] views = {null};

		SwingUtilities.invokeLater(() -> {
			views[0] = new View();
		});

		Thread.sleep(10000);
		assertThat(views[0], CoreMatchers.instanceOf(View.class));
	}

}
